import { ActionTypes } from '../Constants'
import { fetchApi } from '../Utils'

export const checkSession = () => {
  return dispatch => {
    dispatch({
      type: ActionTypes.USER_AUTHENTICATED,
      session: localStorage.getItem('session') || ''
    })
  }
}

export const requestUserData = () => {
  return dispatch => {
    fetchApi('/api/auth/whoami.json', {}).then(rsp => {
      if (rsp.status === 'ok') {
        dispatch({
          type: ActionTypes.USER_WHOAMI_RECEIVED,
          whoami: rsp.data.whoami
        })
      }
    })
  }
}

export const userNameUpdate = (name) => {
  return fetchApi('/api/user/update-name.json', {
    name: name
  })
}

export const userNameUpdated = (name) => {
  return dispatch => {
    dispatch({
      type: ActionTypes.USER_NAME_UPDATED,
      name: name
    })
  }
}