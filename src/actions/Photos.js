import { ActionTypes } from '../Constants'
import { fetchApi } from '../Utils'

export const findRemovedPhotos = () => {
  return dispatch => {
    fetchApi('/api/gathering-photos/find-removed.json', {}).then(rsp => {
      if (rsp.status === 'ok') {
        dispatch({
          type: ActionTypes.TRASH_PHOTOS_RECEIVED,
          data: rsp.data.gatheringPhotos
        })
      }
    })
  }
}

export const removePhotoPermanently = (gatheringPhotoId) => {
  return dispatch => {
      fetchApi('/api/gathering-photos/remove-one.json', {
        gatheringPhotoId: gatheringPhotoId
      }).then(rsp => {
        if (rsp.status === 'ok') {
          dispatch({
            type: ActionTypes.TRASH_PHOTO_REMOVED,
            gatheringPhotoId: gatheringPhotoId
          })
        }
      })
  }
}

export const photoNameUpdate = (name, gatheringPhotoId) => {
  return fetchApi('/api/gathering-photos/update-custom-name.json', {
    customName: name,
    gatheringPhotoId: gatheringPhotoId
  })
}

export const photoNameUpdated = (name, gatheringPhotoId) => {
  return {
    type: ActionTypes.GATHERING_PHOTO_NAME_UPDATED,
    customName: name,
    gatheringPhotoId: gatheringPhotoId
  }
}

export const findPhotosByGathering = (gatheringId) => {
  return dispatch => {
    fetchApi('/api/gathering-photos/find-by-gathering.json', {
      gatheringId: gatheringId
    }).then(rsp => {
      if (rsp.status === 'ok') {
        dispatch({
          type: ActionTypes.GATHERING_PHOTOS_RECEIVED,
          data: rsp.data.gatheringPhotos
        })
      }
    })
  }
}

export const removePhoto = (gatheringPhotoId) => {
  return dispatch => {
    fetchApi('/api/gathering-photos/remove-to-trash.json', {
      gatheringPhotoId: gatheringPhotoId
    }).then(rsp => {
      if (rsp.status === 'ok') {
        dispatch({
          type: ActionTypes.GATHERING_PHOTO_REMOVED,
          gatheringPhotoId: rsp.data.gatheringPhotoId
        })
        dispatch({
          type: ActionTypes.POPUP_CLOSE
        })
      }
    })
  }
}


export const removePhotoAsLeader = (gatheringPhotoId, gatheringId) => {
  return dispatch => {
    fetchApi('/api/gathering-photos/remove-to-trash-as-leader.json', {
      gatheringPhotoId: gatheringPhotoId,
      gatheringId: gatheringId
    }).then(rsp => {
      if (rsp.status === 'ok') {
        dispatch({
          type: ActionTypes.GATHERING_PHOTO_REMOVED,
          gatheringPhotoId: rsp.data.gatheringPhotoId
        })
        dispatch({
          type: ActionTypes.POPUP_CLOSE
        })
      }
    })
  }
}

export const publishPhoto = (gatheringPhotoId) => {
  return fetchApi('/api/gathering-photos/publish.json', {
    gatheringPhotoId: gatheringPhotoId
  })
}

export const photoInserted = (gatheringPhoto) => {
  return {
    type: ActionTypes.GATHERING_PHOTO_INSERTED,
    gatheringPhoto: gatheringPhoto
  }
}

