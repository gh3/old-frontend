import { fetchApi } from '../Utils'
import { ActionTypes } from '../Constants'

export const removeMeAsParticpant = (gatheringId) => {
  return fetchApi('/api/gathering-participants/remove-me.json', {
    gatheringId: gatheringId
  })
}

export const findParticipantsByGathering = (gatheringId) => {
  return dispatch => {
    fetchApi('/api/gathering-participants/find-by-gathering.json', {
      gatheringId: gatheringId
    }).then(rsp => {
      if (rsp.status === 'ok') {
        dispatch({
          type: ActionTypes.GATHERING_PARTICIPANTS_RECEIVED,
          data: rsp.data.gatheringParticipants
        })
      }
    })
  }
}

export const removeOneParticipant = (userId, gatheringId) => {
  return dispatch => {
    fetchApi('/api/gathering-participants/remove-user.json', {
      userId: userId,
      gatheringId: gatheringId
    }).then(rsp => {
      if (rsp.status === 'ok') {
        dispatch({
          type: ActionTypes.GATHERING_PARTICIPANT_REMOVED,
          userId: userId
        })
      }
    })
  }
}
