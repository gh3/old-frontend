import { ActionTypes } from '../Constants'
import { fetchApi } from '../Utils'

export const requestAreacodes = () => {
  return dispatch => {
    fetchApi('/api/areacodes/find.json', {}).then(rsp => {
      if (rsp.status === 'ok') {
        dispatch({
          type: ActionTypes.AREACODES_DATA_RECEIVED,
          areacodes: rsp.data.areacodes
        })
      }
    })
  }
}
