import { ActionTypes } from '../Constants'

/* eslint-disable no-undef */
export const startCamera = () => {
  return dispatch => {
    CameraPreview.startCamera({
      x: 0,
      y: 0,
      width: window.screen.width / window.devicePixelRatio,
      height: window.screen.height / window.devicePixelRatio,
      camera: CameraPreview.CAMERA_DIRECTION.BACK,
      toBack: true
    }, () => {
      document.body.style.backgroundColor = 'transparent'
      dispatch({
        type: ActionTypes.CAMERA_PREVIEW_STARTED
      })
    })
  }
}
/* eslint-enable no-undef */

