import { ActionTypes } from '../Constants'
import { fetchApi } from '../Utils'

export const sendInviteViaSms = (areacode, phone, gatheringId) => {
  return fetchApi('/api/gathering-invites/send-via-sms.json', {
    areacode: areacode,
    phone: phone,
    gatheringId: gatheringId
  })
}

export const inviteInserted = (gatheringInvite) => {
  return {
    type: ActionTypes.GATHERING_INVITE_INSERTED,
    gatheringInvite: gatheringInvite
  }
}

export const findInvitesByGathering = (gatheringId) => {
  return dispatch => {
    fetchApi('/api/gathering-invites/find-by-gathering.json', {
      gatheringId: gatheringId
    }).then(rsp => {
      if (rsp.status === 'ok') {
        dispatch({
          type: ActionTypes.GATHERING_INVITES_RECEIVED,
          data: rsp.data.gatheringInvites
        })
      }
    })
  }
}

export const removeOneGatheringInvite = (gatheringInviteId, gatheringId) => {
  return dispatch => {
    fetchApi('/api/gathering-invites/remove-one.json', {
      gatheringInviteId: gatheringInviteId,
      gatheringId: gatheringId
    }).then(rsp => {
      if (rsp.status === 'ok') {
        dispatch({
          type: ActionTypes.GATHERING_INVITE_REMOVED,
          gatheringInviteId: gatheringInviteId
        })
      }
    })
  }
}

export const checkBelonging = (token) => {
  return fetchApi('/api/gathering-invites/check-belonging.json', {
    token: token
  })
}

export const registerViaInvite = (areacode, phone, token) => {
  return fetchApi('/api/gathering-invites/register.json', {
    areacode: areacode,
    phone: phone,
    token: token,
    doublecheck: 1
  })
}

export const loginViaInvite = (areacode, phone, token) => {
  return fetchApi('/api/gathering-invites/login.json', {
    areacode: areacode,
    phone: phone,
    token: token
  })
}

export const respondOnInvite = (token, respond) => {
  return fetchApi('/api/gathering-invites/respond-on-token.json', {
    token: token,
    respond: respond
  })
}
