import { ActionTypes } from '../Constants'

export const openPopup = (popupType, buttons, data) => {
  return {
    type: ActionTypes.POPUP_OPEN,
    popupType: popupType,
    data: data,
    buttons: buttons
  }
}

export const closePopup = () => {
  return {
    type: ActionTypes.POPUP_CLOSE
  }
}

export const setPopupButtons = (buttons) => {
  return {
    type: ActionTypes.POPUP_BUTTONS_SET,
    buttons: buttons
  }
}

export const setPopupData = (data) => {
  return {
    type: ActionTypes.POPUP_DATA_SET,
    data: data
  }
}
