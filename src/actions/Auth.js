import { ActionTypes } from '../Constants'
import { fetchApi } from '../Utils'

export const storeReset = () => {
  return {
    type: ActionTypes.STORE_RESET
  }
}

export const login = (areacode, phone, password) => {
  return fetchApi('/api/auth/login.json', {
    areacode: areacode,
    phone: phone,
    password: password
  })
}

export const register = (areacode, phone, password, doublecheck) => {
  return fetchApi('/api/auth/register.json', {
    areacode: areacode,
    phone: phone,
    password: password,
    doublecheck: doublecheck
  })
}

export const memorizeAuthData = (areacode, phone, password) => {
  return {
    type: ActionTypes.USER_AUTH_STORE,
    data: {
      areacode: areacode,
      phone: phone,
      password: password
    }
  }
}

export const setSession = (session) => {
  return {
    type: ActionTypes.USER_SESSION_SET,
    session: session
  }
}

export const unsetSession = () => {
  return {
    type: ActionTypes.USER_SESSION_UNSET
  }
}

export const recoverPassword = (areacode, phone) => {
  return fetchApi('/api/auth/password-recovery.json', {
    areacode: areacode,
    phone: phone
  })
}

export const checkRecoveryToken = (token) => {
  return fetchApi('/api/auth/check-recovery-token.json', {
    token: token
  })
}

export const passwordSet = (token, newPassword, passwordRepeat) => {
  return fetchApi('/api/auth/password-set.json', {
    token: token,
    newPassword: newPassword,
    passwordRepeat: passwordRepeat
  })
}
