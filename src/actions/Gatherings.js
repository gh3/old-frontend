import { ActionTypes } from '../Constants'
import { fetchApi } from '../Utils'

export const findWhereParticipate = () => {
  return dispatch => {
    fetchApi('/api/gatherings/find-where-participate.json', {}).then(rsp => {
      if (rsp.status === 'ok') {
        dispatch({
          type: ActionTypes.GATHERINGS_WHERE_PARTICIPATE,
          gatherings: rsp.data.gatherings
        })
      }
    })
  }
}

export const findOneGathering = (gatheringId) => {
  return fetchApi('/api/gatherings/find-one.json', {
    gatheringId: gatheringId
  })
}

export const createGathering = (title) => {
  return fetchApi('/api/gatherings/insert.json', {
    title: title
  })
}

export const gatheringDataReceived = (gatheringData) => {
  return {
    type: ActionTypes.GATHERING_DATA_RECEIVED,
    data: gatheringData
  }
}

export const gatheringWillUnmount = () => {
  return {
    type: ActionTypes.GATHERING_WILL_UNMOUNT
  }
}

export const removedOneGathering = (gatheringId) => {
  return {
    type: ActionTypes.GATHERINGS_ONE_REMOVED,
    gatheringId: gatheringId
  }
}

export const prepareForRemoving = (gatheringId) => {
  return fetchApi('/api/gatherings/prepare-for-removing.json', {
    gatheringId: gatheringId
  })
}

export const gatheringRemovingSet = (gatheringId, removingDate) => {
  return {
    type: ActionTypes.GATHERING_REMOVING_SET,
    gatheringId: gatheringId,
    removingDate: removingDate
  }
}

export const cancelGatheringRemoving = (gatheringId) => {
  return dispatch => {
    fetchApi('/api/gatherings/cancel-removing.json', {
      gatheringId: gatheringId
    }).then(rsp => {
      if (rsp.status === 'ok') {
        dispatch({
          type: ActionTypes.GATHERING_REMOVING_UNSET,
          gatheringId: gatheringId
        })
      }
    })
  }
}
