/*eslint-disable no-var, one-var */
import fetch from 'isomorphic-fetch'
import { polyfill } from 'es6-promise'
polyfill()

export const fetchApi = (url, data) => {
  var fetchUrl = process.env.REACT_APP_BASE_URL + url
  return fetch(fetchUrl, {
    method: 'post',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'Session': localStorage.getItem('session')
    },
    body: JSON.stringify(data)
  }).then(rsp => {
    return rsp.json()
  })
}

export const fetchPhoto = (url) => {
  var fetchUrl = process.env.REACT_APP_BASE_URL + url
  return fetch(fetchUrl, {
    method: 'get',
    credentials: 'include',
    headers: {
      'Session': localStorage.getItem('session')
    }
  })
}

export const arrayBufferToBase64 = (buffer) => {
  var binary = ''
  var bytes = [].slice.call(new Uint8Array(buffer))
  bytes.forEach((b) => binary += String.fromCharCode(b))
  return window.btoa(binary);
}

export const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
  const byteCharacters = atob(b64Data)
  const byteArrays = []
  
  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize)
    
    const byteNumbers = new Array(slice.length)
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i)
    }
    
    const byteArray = new Uint8Array(byteNumbers)
    
    byteArrays.push(byteArray)
  }
  
  const blob = new Blob(byteArrays, {type: contentType});
  return blob
}

export const generateGuid = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    var r = Math.random() * 16 | 0
    var v = c === 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16)
  })
}
