import { applyMiddleware, createStore } from 'redux'
import reducers from './reducers'
import logger from 'redux-logger'
import thunk from 'redux-thunk'

let middleware
if (process.env.NODE_ENV === 'development') {
  middleware = applyMiddleware(thunk, logger)
} else {
  middleware = applyMiddleware(thunk)
}

const store = createStore(reducers, middleware)
export default store
