import { ActionTypes } from '../Constants'

const initialTrashStore = {
  removedPhotos: []
}

const trashReducer = function(store = initialTrashStore, action) {
  switch(action.type) {

    case ActionTypes.STORE_RESET: {
      return initialTrashStore
    }

    case ActionTypes.TRASH_PHOTOS_RECEIVED: {
      return { ...store, removedPhotos: action.data }
    }

    case ActionTypes.TRASH_PHOTO_REMOVED: {
      const removedPhotos = store.removedPhotos.filter(function(elm) {
        return elm._id !== action.gatheringPhotoId
      })
      return { ...store, removedPhotos: removedPhotos }
    }

    default: {
      return store
    }
  }
}

export default trashReducer
