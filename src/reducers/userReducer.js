import { ActionTypes } from '../Constants'

const initialUserStore = {
  auth: {},
  session: '',
  isChecked: false,
  whoami: {}
}

const userReducer = function(store = initialUserStore, action) {
  switch(action.type) {

    case ActionTypes.STORE_RESET: {
      localStorage.removeItem('session');
      return initialUserStore
    }

    case ActionTypes.USER_AUTHENTICATED: {
      return { ...store, session: action.session, isChecked: true }
    }

    case ActionTypes.USER_AUTH_STORE: {
      return { ...store, auth: action.data }
    }

    case ActionTypes.USER_SESSION_SET: {
      localStorage.setItem('session', action.session);
      return { ...store, auth: initialUserStore.auth, session: action.session }
    }

    case ActionTypes.USER_SESSION_UNSET: {
      localStorage.removeItem('session');
      return { ...store, session: initialUserStore.session, whoami: initialUserStore.whoami }
    }

    case ActionTypes.USER_WHOAMI_RECEIVED: {
      return { ...store, whoami: action.whoami }
    }

    case ActionTypes.USER_NAME_UPDATED: {
      const whoami = { ...store.whoami }
      whoami.name = action.name
      return { ...store, whoami: whoami }
    }

    default: {
      return store
    }
  }
}

export default userReducer
