import { ActionTypes } from '../Constants'

const initialCordovaStore = {
  cameraRunning: false
}

const cordovaReducer = function(store = initialCordovaStore, action) {
  switch(action.type) {
    case ActionTypes.STORE_RESET: {
      return initialCordovaStore
    }

    case ActionTypes.CAMERA_PREVIEW_STARTED: {
      return { ...store, cameraRunning: true }
    }

    case ActionTypes.CAMERA_PREVIEW_STOPPED: {
      return { ...store, cameraRunning: false }
    }

    default: {
      return store
    }
  }
}

export default cordovaReducer
