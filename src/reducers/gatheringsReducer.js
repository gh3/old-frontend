import { ActionTypes } from '../Constants'

const initialGatheringsStore = {
  gatherings: []
}

const gatheringsReducer = function(store = initialGatheringsStore, action) {
  switch(action.type) {
    case ActionTypes.STORE_RESET: {
      return initialGatheringsStore
    }

    case ActionTypes.GATHERINGS_WHERE_PARTICIPATE: {
      return { ...store, gatherings: action.gatherings }
    }

    case ActionTypes.GATHERINGS_ONE_REMOVED: {
      const gatherings = store.gatherings.filter(function(elm) {
        return elm._id !== action.gatheringId
      })
      return { ...store, gatherings: gatherings }
    }

    case ActionTypes.GATHERING_REMOVING_SET: {
      const gatherings = store.gatherings.map(function(elm) {
        if (elm._id === action.gatheringId) {
          elm.removingDate = action.removingDate
        }
        return elm
      })
      return { ...store, gatherings: gatherings }
    }

    case ActionTypes.GATHERING_REMOVING_UNSET: {
      const gatherings = store.gatherings.map(function(elm) {
        if (elm._id === action.gatheringId) {
          delete elm.removingDate
        }
        return elm
      })
      return { ...store, gatherings: gatherings }
    }

    default: {
      return store
    }
  }
}

export default gatheringsReducer
