import { combineReducers } from 'redux'
import userReducer from './userReducer'
import gatheringReducer from './gatheringReducer'
import gatheringsReducer from './gatheringsReducer'
import popupReducer from './popupReducer'
import areacodesReducer from './areacodesReducer'
import trashReducer from './trashReducer'
import cordovaReducer from './cordovaReducer'

const reducers = combineReducers({
  userStore: userReducer,
  gatheringStore: gatheringReducer,
  gatheringsStore: gatheringsReducer,
  popupStore: popupReducer,
  areacodesStore: areacodesReducer,
  trashStore: trashReducer,
  cordovaStore: cordovaReducer
})

export default reducers
