import { ActionTypes } from '../Constants'

const initialPopupStore = {
  visible: false,
  popupType: '',
  data: {},
  buttons: []
}

const popupReducer = function(store = initialPopupStore, action) {
  switch(action.type) {

    case ActionTypes.STORE_RESET: {
      return initialPopupStore
    }

    case ActionTypes.POPUP_OPEN: {
      return { ...store,
        visible: true,
        popupType: action.popupType,
        data: action.data,
        buttons: action.buttons
      }
    }

    case ActionTypes.POPUP_CLOSE: {
      return initialPopupStore
    }

    case ActionTypes.POPUP_BUTTONS_SET: {
      return { ...store, buttons: action.buttons }
    }

    case ActionTypes.POPUP_DATA_SET: {
      return { ...store, data: action.data }
    }

    default: {
      return store
    }
  }
}

export default popupReducer
