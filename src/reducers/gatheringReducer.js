import { ActionTypes } from '../Constants'

const initialGatheringStore = {
  gathering: {},
  gatheringPhotos: [],
  gatheringParticipants: [],
  gatheringInvites: []
}

const gatheringReducer = function(store = initialGatheringStore, action) {
  switch(action.type) {
    case ActionTypes.STORE_RESET: {
      return initialGatheringStore
    }

    case ActionTypes.GATHERING_DATA_RECEIVED: {
      return { ...store, gathering: action.data }
    }

    case ActionTypes.GATHERING_PHOTOS_RECEIVED: {
      return { ...store, gatheringPhotos: action.data }
    }

    case ActionTypes.GATHERING_PHOTO_INSERTED: {
      const gatheringPhotos = [].concat(store.gatheringPhotos)
      gatheringPhotos.unshift(action.gatheringPhoto)
      return { ...store, gatheringPhotos: gatheringPhotos }
    }

    case ActionTypes.GATHERING_PHOTO_NAME_UPDATED: {
      const gatheringPhotos = [].concat(store.gatheringPhotos)
      const i = gatheringPhotos.findIndex(elm => elm._id === action.gatheringPhotoId)
      gatheringPhotos[i].customName = action.customName
      return { ...store, gatheringPhotos: gatheringPhotos }
    }

    case ActionTypes.GATHERING_PHOTO_REMOVED: {
      const gatheringPhotos = store.gatheringPhotos.filter(function(elm) {
        return elm._id !== action.gatheringPhotoId
      })
      return { ...store, gatheringPhotos: gatheringPhotos }
    }

    case ActionTypes.GATHERING_PARTICIPANTS_RECEIVED: {
      return { ...store, gatheringParticipants: action.data }
    }

    case ActionTypes.GATHERING_PARTICIPANT_REMOVED: {
      const gatheringParticipants = store.gatheringParticipants.filter(function(elm) {
        return elm._id !== action.userId
      })
      return { ...store, gatheringParticipants: gatheringParticipants }
    }

    case ActionTypes.GATHERING_INVITES_RECEIVED: {
      return { ...store, gatheringInvites: action.data }
    }

    case ActionTypes.GATHERING_INVITE_REMOVED: {
      const gatheringInvites = store.gatheringInvites.filter(function(elm) {
        return elm._id !== action.gatheringInviteId
      })
      return { ...store, gatheringInvites: gatheringInvites }
    }

    case ActionTypes.GATHERING_INVITE_INSERTED: {
      const gatheringInvites = [].concat(store.gatheringInvites)
      gatheringInvites.push(action.gatheringInvite)
      return { ...store, gatheringInvites: gatheringInvites }
    }

    case ActionTypes.GATHERING_WILL_UNMOUNT: {
      return initialGatheringStore
    }

    case ActionTypes.GATHERING_REMOVING_SET: {
      const gathering = store.gathering
      gathering.removingDate = action.removingDate
      return { ...store, gathering: gathering }
    }

    case ActionTypes.GATHERING_REMOVING_UNSET: {
      const gathering = store.gathering
      delete gathering.removingDate
      return { ...store, gathering: gathering }
    }

    default: {
      return store
    }
  }
}

export default gatheringReducer
