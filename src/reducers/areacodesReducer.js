import { ActionTypes } from '../Constants'

const initialAreacodesStore = []

const areacodesReducer = function(store = initialAreacodesStore, action) {
  switch(action.type) {
    case ActionTypes.STORE_RESET: {
      return initialAreacodesStore
    }

    case ActionTypes.AREACODES_DATA_RECEIVED: {
      return [ ...store, ...action.areacodes ]
    }

    default: {
      return store
    }
  }
}

export default areacodesReducer
