import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { PopupTypes } from '../../Constants'
import { findWhereParticipate } from '../../actions/Gatherings'
import { openPopup } from '../../actions/Popup'

class Dashboard extends Component {

  constructor() {
    super()

    this.handleCreateGathering = this.handleCreateGathering.bind(this)
  }

  componentDidMount() {
    this.props.findWhereParticipate()
  }

  handleCreateGathering() {
    this.props.openPopup(PopupTypes.CREATE_GATHERING)
  }

  render() {
    return (
      <div>
        <h1>Welcome to the app!</h1>
        <div>{this.props.gatherings.length ? '' : 'You don\'t have any gatherings yet.'}</div>
        <a className="new-gathering-btn" onClick={this.handleCreateGathering}>Create New Gathering</a>
        <div>{this.props.gatherings.length ? 'Here are your gatherings:' : ''}</div>
        <div>
          {this.props.gatherings.map(function(gathering) {
            return (
              <div key={gathering._id}>
                <Link to={'/gathering/' + gathering._id} className="gathering-list">{gathering.title}</Link>
                { gathering.removingDate ? (<strong style={{marginLeft: '10px'}}>removed soon</strong>) : ''}
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}

Dashboard.propTypes = {
  gatherings: PropTypes.array.isRequired,
  findWhereParticipate: PropTypes.func.isRequired,
  openPopup: PropTypes.func.isRequired
}

const mapStoreToProps = function(store) {
  return {
    gatherings: store.gatheringsStore.gatherings
  }
}

const mapDispatchToProps = dispatch => {
  return {
    findWhereParticipate: () => {
      dispatch(findWhereParticipate())
    },
    openPopup: (popupType) => {
      dispatch(openPopup(popupType))
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(Dashboard)
