import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Switch, Route } from 'react-router-dom'
import routes from '../../Routes'
import { find } from 'lodash'
import { findOneGathering, gatheringDataReceived, gatheringWillUnmount } from '../../actions/Gatherings'
import { GatheringModes } from '../../Constants'

class Gathering extends Component {

  constructor() {
    super()
    this.state = {
      mode: GatheringModes.INITIALIZATION,
      msg: 'Loading gathering...'
    }
  }

  componentWillMount() {
    if (this.props.match.params.gatheringId) {
      this.props.findOneGathering(this.props.match.params.gatheringId).then(rsp => {
        if (rsp.status === 'ok') {
          this.setState({
            mode: GatheringModes.GATHERING_OK
          })
          this.props.gatheringDataReceived(rsp.data.gathering)
        } else {
          this.setState({
            mode: GatheringModes.ERROR,
            msg: rsp.msg
          })
        }
      })
    }
  }

  componentWillUnmount() {
    this.props.gatheringWillUnmount()
  }

  renderRoute(route) {
    return (
      <Route key={route.id} path={route.path} component={route.component} exact={!!route.exact} />
    )
  }

  render() {
    if (this.state.mode === GatheringModes.GATHERING_OK) {
      const gatheringRoutes = find(routes, {id: 'gathering'}).routes
      return (
        <div>
          <div>Gathering: <strong><Link to={'/gathering/' + this.props.gathering._id}>{this.props.gathering.title}</Link></strong></div>
          <hr />
          <Switch>
            {gatheringRoutes.map(route => this.renderRoute(route))}
          </Switch>
        </div>
      )
    }

    return (
      <div>{this.state.msg}</div>
    )
  }
}

Gathering.propTypes = {
  gathering: PropTypes.object.isRequired,
  findOneGathering: PropTypes.func.isRequired,
  gatheringDataReceived: PropTypes.func.isRequired,
  gatheringWillUnmount: PropTypes.func.isRequired
}

const mapStoreToProps = function(store) {
  return {
    gathering: store.gatheringStore.gathering
  }
}

const mapDispatchToProps = dispatch => {
  return {
    findOneGathering: (gatheringId) => {
      return findOneGathering(gatheringId)
    },
    gatheringDataReceived: (gatheringData) => {
      dispatch(gatheringDataReceived(gatheringData))
    },
    gatheringWillUnmount: () => {
      dispatch(gatheringWillUnmount())
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(Gathering)
