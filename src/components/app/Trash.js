import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { findIndex } from 'lodash'
import { findRemovedPhotos, removePhotoPermanently } from '../../actions/Photos'
import { fetchPhoto, arrayBufferToBase64 } from '../../Utils'

class Trash extends Component {

  constructor() {
    super()

    this.state = {
      removedPhotos: []
    }

    this.permanentlyRemovePhoto = this.permanentlyRemovePhoto.bind(this)
    this.renderPhotoName = this.renderPhotoName.bind(this)
    this.renderPhoto = this.renderPhoto.bind(this)
  }

  componentWillMount() {
    this.props.findRemovedPhotos()
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.removedPhotos !== nextProps.removedPhotos) {

      this.setState({
        removedPhotos: nextProps.removedPhotos
      })

      nextProps.removedPhotos.forEach(gatheringPhoto => {
        this.requestImageSource(gatheringPhoto)
      })
    }
  }

  requestImageSource(gatheringPhoto) {
    // https://medium.com/front-end-hacking/fetching-images-with-the-fetch-api-fb8761ed27b2
    const photoCropURL = '/photo/' + gatheringPhoto.guid + '_300x300.jpg' 

    fetchPhoto(photoCropURL, {}).then(response => {
      response.arrayBuffer().then((buffer) => {
        let base64Flag = 'data:' + gatheringPhoto.type + ';base64,'
        let imageStr = arrayBufferToBase64(buffer)
        gatheringPhoto.src = base64Flag + imageStr

        let gatheringPhotos = this.state.removedPhotos
        let gatheringPhotoIndex = findIndex(gatheringPhotos, { _id: gatheringPhoto._id })
        gatheringPhotos[gatheringPhotoIndex] = gatheringPhoto

        this.setState({
          removedPhotos: gatheringPhotos
        })
      })
    })
  }

  permanentlyRemovePhoto(gatheringPhotoId) {
    this.props.removePhotoPermanently(gatheringPhotoId)
  }

  renderPhotoName(gatheringPhoto) {
    let photoName = gatheringPhoto.customName ? gatheringPhoto.customName : gatheringPhoto.name

    return (
      <div>
        <a onClick={this.permanentlyRemovePhoto.bind(this, gatheringPhoto._id)}>X</a> <strong className="gathering-photo-name">{photoName}</strong>
      </div>
    )
  }

  renderPhoto(gatheringPhoto) {
    return (
      <div key={gatheringPhoto._id}>
        <img src={gatheringPhoto.src} className="gathering-photo" alt="" />
        {this.renderPhotoName(gatheringPhoto)}
      </div>
    )
  }

  render() {
    return (
      <div>
        <span>Trash</span>
        <div>
          {this.state.removedPhotos.map(this.renderPhoto)}
        </div>
      </div>
    )
  }
}

Trash.propTypes = {
  removedPhotos: PropTypes.array.isRequired,
  findRemovedPhotos: PropTypes.func.isRequired,
  removePhotoPermanently: PropTypes.func.isRequired
}

const mapStoreToProps = function(store) {
  return {
    removedPhotos: store.trashStore.removedPhotos
  }
}

const mapDispatchToProps = dispatch => {
  return {
    findRemovedPhotos: () => {
      dispatch(findRemovedPhotos())
    },
    removePhotoPermanently: (gatheringPhotoId) => {
      dispatch(removePhotoPermanently(gatheringPhotoId))
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(Trash)
