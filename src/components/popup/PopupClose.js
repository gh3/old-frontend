import React, { Component } from 'react'
import { closePopup } from '../../actions/Popup'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

class PopupClose extends Component {

  render() {
    return (
      <a className="popup__close" onClick={this.props.closePopup}>X</a>
    )
  }
}

PopupClose.propTypes = {
  closePopup: PropTypes.func.isRequired
}

const mapDispatchToProps = dispatch => {
  return {
    closePopup: () => {
      dispatch(closePopup())
    }
  }
}

export default connect(null, mapDispatchToProps)(PopupClose)
