import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { PopupTypes } from '../../Constants'
import Prompt from './popupTypes/Prompt'
import CreateGathering from './popupTypes/CreateGathering'
import RegisterDoublecheck from './popupTypes/RegisterDoublecheck'
import GatheringInvite from './popupTypes/GatheringInvite'
import AskForName from './popupTypes/AskForName'
import EditPhotoName from './popupTypes/EditPhotoName'
import { closePopup } from '../../actions/Popup'

class Popup extends Component {

  constructor() {
    super()

    this.handleWindowPopState = this.handleWindowPopState.bind(this)
  }

  componentDidMount() {
    window.onpopstate = this.handleWindowPopState
  }

  handleWindowPopState() {
    if (this.props.visible) {
      this.props.closePopup()
    }
  }

  render() {
    if (!this.props.visible) {
      return null
    }

    let popupType = ''

    switch(this.props.popupType) {
      case PopupTypes.PROMPT:
        popupType = <Prompt />
        break
      case PopupTypes.REGISTRATION_DOUBLECHECK:
        popupType = <RegisterDoublecheck />
        break
      case PopupTypes.CREATE_GATHERING:
        popupType = <CreateGathering />
        break
      case PopupTypes.GATHERING_INVITE:
        popupType = <GatheringInvite />
        break
      case PopupTypes.ASK_FOR_NAME:
        popupType = <AskForName />
        break
      case PopupTypes.EDIT_PHOTO_NAME:
        popupType = <EditPhotoName />
        break
      default:
        return null
    }

    return (
      <div className="popup">
        {popupType}
      </div>
    )
  }
}

Popup.propTypes = {
  visible: PropTypes.bool.isRequired,
  popupType: PropTypes.string.isRequired,
  closePopup: PropTypes.func.isRequired
}

const mapStoreToProps = function(store) {
  return {
    visible: store.popupStore.visible,
    popupType: store.popupStore.popupType
  }
}

const mapDispatchToProps = dispatch => {
  return {
    closePopup: () => {
      dispatch(closePopup())
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(Popup)
