import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

class PopupButtons extends Component {

  render() {
    if (!this.props.buttons) {
      return null
    }

    let popupButtons = this.props.buttons.map((button) => {
      return (
        <a key={button.order} onClick={button.callback}>{button.caption}</a>
      )
    })

    return (
      <div className="popup__buttons">{popupButtons}</div>
    )
  }
}

PopupButtons.propTypes = {
  buttons: PropTypes.array.isRequired
}

const mapStoreToProps = function(store) {
  return {
    buttons: store.popupStore.buttons
  }
}

export default connect(mapStoreToProps)(PopupButtons)