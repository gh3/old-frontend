import React, { Component } from 'react'
import PopupButtons from '../PopupButtons'
import PopupClose from '../PopupClose'
import { setPopupButtons, closePopup } from '../../../actions/Popup'
import { userNameUpdate, userNameUpdated } from '../../../actions/User'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { AskForNameModes } from '../../../Constants'

class AskForName extends Component {

  constructor() {
    super()
    this.state = {
      mode: AskForNameModes.STEP_1,
      msg: ''
    }

    this.handleStep2 = this.handleStep2.bind(this)
    this.handleStep3 = this.handleStep3.bind(this)
    this.handleNextName = this.handleNextName.bind(this)
    this.randomNinjaName = this.randomNinjaName.bind(this)
    this.randomIntFromInterval = this.randomIntFromInterval.bind(this)
    this.capitalizeFirstLetter = this.capitalizeFirstLetter.bind(this)
    this.updateName = this.updateName.bind(this)
  }

  componentWillMount() {
    this.props.setPopupButtons([
      {
        order: 1,
        caption: 'OK',
        callback: this.updateName
      }, {
        order: 2,
        caption: 'I hate names',
        callback: this.handleStep2
      }
    ])
  }

  handleStep2() {
    this.props.setPopupButtons([
      {
        order: 1,
        caption: 'OK',
        callback: this.updateName
      }, {
        order: 2,
        caption: 'No',
        callback: this.handleStep3
      }
    ])

    this.setState({
      mode: AskForNameModes.STEP_2
    })
  }

  handleStep3() {
    this.props.setPopupButtons([
      {
        order: 1,
        caption: 'OK',
        callback: this.updateName
      }, {
        order: 2,
        caption: 'Next',
        callback: this.handleNextName
      }
    ])

    this.setState({
      mode: AskForNameModes.STEP_3
    })

    this.handleNextName()
  }

  handleNextName() {
    this.refs.name.value = this.randomNinjaName()
  }

  randomNinjaName() {
    let syllables = ['ka', 'zu', 'mi', 'te', 'ku', 'lu', 'ji', 'ri', 'ki', 'zu', 'me', 'ta', 'rin', 'to', 'mo', 'no', 'ke', 'shi', 'ari', 'chi', 'do', 'ru', 'mei', 'na', 'fu', 'zu']
    let numberOfSyllables = this.randomIntFromInterval(2,4)
    let ninjaName = ''
    for (let i = 0; i < numberOfSyllables; i++) {
      ninjaName = ninjaName + syllables[Math.floor(Math.random() * syllables.length)]
    }
    ninjaName = this.capitalizeFirstLetter(ninjaName)
    return ninjaName
  }

  randomIntFromInterval(min, max) {
    return Math.floor(Math.random()*(max-min+1)+min)
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
  }

  updateName() {
    const userName = this.refs.name.value
    if (!userName) {
      this.setState({
        msg: 'Please enter name!'
      })
      return
    }

    this.setState({
      msg: ''
    })

    this.props.userNameUpdate(userName).then(rsp => {
      if (rsp.status === 'ok') {
        this.props.userNameUpdated(userName)
        this.props.closePopup()
      } else if (rsp.status === 'error') {
        this.setState({
          msg: rsp.msg
        })
      }
    })
  }

  render() {
    let asking
    if (this.state.mode === AskForNameModes.STEP_1) {
      asking = 'No one knows who you are yet! Please tell us your name:'
    } else if (this.state.mode === AskForNameModes.STEP_2) {
      asking = 'Do you have a nickname. Your friends will recognize you like this:'
    } else if (this.state.mode === AskForNameModes.STEP_3) {
      asking = 'I have an idea. You can have a Ninja name! This one suits you:'
    }

    return (
      <div className="popup--ask-for-name">
        <PopupClose />
        <div className="popup__content">
          <div className="active">Hey Snowflake!!</div>
          <div>{asking}</div>
          <span>New name:</span>
          <input placeholder="New name" className="form-block" type="text" ref="name" />
          <div>Hit OK when done.</div>
          <div>{this.state.msg}</div>
        </div>
        <PopupButtons />
      </div>
    )
  }   
}

AskForName.propTypes = {
  setPopupButtons: PropTypes.func.isRequired,
  closePopup: PropTypes.func.isRequired,
  userNameUpdate: PropTypes.func.isRequired,
  userNameUpdated: PropTypes.func.isRequired
}

const mapDispatchToProps = dispatch => {
  return {
    setPopupButtons: (buttons) => {
      dispatch(setPopupButtons(buttons))
    },
    closePopup: () => {
      dispatch(closePopup())
    },
    userNameUpdate: (name) => {
      return userNameUpdate(name)
    },
    userNameUpdated: (name) => {
      dispatch(userNameUpdated(name))
    }
  }
}

export default connect(null, mapDispatchToProps)(AskForName)
