import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PopupButtons from '../PopupButtons'
import PopupClose from '../PopupClose'
import { connect } from 'react-redux'
import { setPopupButtons, closePopup } from '../../../actions/Popup'
import { sendInviteViaSms, inviteInserted } from '../../../actions/Invites'

class GatheringInvite extends Component {

  constructor() {
    super()
    this.state = {
      msg: ''
    }

    this.handleCancel = this.handleCancel.bind(this)
    this.handleInvite = this.handleInvite.bind(this)
    this.renderAreacodes = this.renderAreacodes.bind(this)
  }

  componentWillMount() {
    this.props.setPopupButtons([
      {
        order: 1,
        caption: 'Invite',
        callback: this.handleInvite
      }, {
        order: 2,
        caption: 'Cancel',
        callback: this.handleCancel
      }
    ])
  }

  handleCancel() {
    this.props.closePopup()
  }

  handleInvite(e) {
    if (e.keyCode && e.keyCode !== 13) {
      return
    }

    if (!this.refs.areacode.value) {
      this.setState({
        msg: 'Please enter areacode!'
      })
      return
    }

    if (!this.refs.phone.value) {
      this.setState({
        msg: 'Please enter phone!'
      })
      return
    }

    this.setState({
      msg: ''
    })

    this.props.sendInviteViaSms(this.refs.areacode.value, this.refs.phone.value, this.props.data.gatheringId).then(rsp => {
      if (rsp.status === 'ok') {
        this.setState({
          msg: rsp.msg
        })

        this.refs.phone.value = ''

        this.props.inviteInserted(rsp.data.gatheringInvite)

        this.props.setPopupButtons([
          {
            order: 1,
            caption: 'Invite another',
            callback: this.handleInvite
          }, {
            order: 2,
            caption: 'Cancel',
            callback: this.handleCancel
          }
        ])

      } else if (rsp.status === 'error') {
        this.setState({
          msg: rsp.msg
        })
      }
    })
  }

  renderAreacodes() {
    let areacodesOptions = this.props.areacodes.map((areacode) => {
      return (
        <option key={areacode._id} value={areacode.areacode}>{areacode.title}</option>
      )
    })

    return (
      <select className="form-block" ref="areacode">{areacodesOptions}</select>
    )
  }

  render() {
    return (
      <div className="popup--gathering-invite">
        <PopupClose />
        <div className="popup__content">
          <div>
            <div>
              <label>
                <span>Areacode</span>
                {this.renderAreacodes()}
              </label>
            </div>
            <div>
              <label>
                <span>Phone</span>
                <input type="tel" ref="phone" onKeyDown={this.handleInvite} className="form-block" placeholder="Phone" />
              </label>
            </div>
            <div>{this.state.msg}</div>
          </div>
        </div>
        <PopupButtons />
      </div>
    )
  }
}

GatheringInvite.propTypes = {
  data: PropTypes.object.isRequired,
  areacodes: PropTypes.array.isRequired,
  setPopupButtons: PropTypes.func.isRequired,
  closePopup: PropTypes.func.isRequired,
  sendInviteViaSms: PropTypes.func.isRequired,
  inviteInserted: PropTypes.func.isRequired
}

const mapStoreToProps = function(store) {
  return {
    data: store.popupStore.data,
    areacodes: store.areacodesStore
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setPopupButtons: (buttons) => {
      dispatch(setPopupButtons(buttons))
    },
    closePopup: () => {
      dispatch(closePopup())
    },
    sendInviteViaSms: (areacode, phone, gatheringId) => {
      return sendInviteViaSms(areacode, phone, gatheringId)
    },
    inviteInserted: (gatheringInvite) => {
      dispatch(inviteInserted(gatheringInvite))
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(GatheringInvite)
