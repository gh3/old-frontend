import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PopupButtons from '../PopupButtons'
import PopupClose from '../PopupClose'
import { connect } from 'react-redux'

class RegisterDoublecheck extends Component {

  render() {
    return (
      <div className="popup--register-doublecheck">
        <PopupClose />
        <div className="popup__content">
          <div>We will be verifying the phone number:</div>
          <strong className="form-block">{this.props.data.phoneSMS}</strong>
          <div>Is this OK, or would you like to edit the number?</div>
        </div>
        <PopupButtons />
      </div>
    )
  }
}

RegisterDoublecheck.propTypes = {
  data: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
  return {
    data: store.popupStore.data
  }
}

export default connect(mapStoreToProps)(RegisterDoublecheck)
