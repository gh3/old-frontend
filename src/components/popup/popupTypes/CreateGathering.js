import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PopupButtons from '../PopupButtons'
import PopupClose from '../PopupClose'
import { setPopupButtons, closePopup } from '../../../actions/Popup'
import { createGathering } from '../../../actions/Gatherings'
import { connect } from 'react-redux'

class CreateGathering extends Component {

  constructor() {
    super()
    this.state = {
      msg: ''
    }

    this.handleCancel = this.handleCancel.bind(this)
    this.insertGathering = this.insertGathering.bind(this)
  }

  componentWillMount() {
    this.props.setPopupButtons([
      {
        order: 1,
        caption: 'OK',
        callback: this.insertGathering
      }, {
        order: 2,
        caption: 'Cancel',
        callback: this.handleCancel
      }
    ])
  }

  insertGathering() {
    if (!this.refs.title.value) {
      this.setState({
        msg: 'Please enter title!'
      })
      return
    }

    this.setState({
      msg: ''
    })

    this.props.createGathering(this.refs.title.value).then(rsp => {
      if (rsp.status === 'ok') {
        this.props.closePopup()
        this.context.router.history.push('/gathering/' + rsp.data.gathering._id)
      } else if (rsp.status === 'error') {
        this.setState({
          msg: rsp.msg
        })
      }
    })
  }

  handleCancel() {
    this.props.closePopup()
  }

  render() {
    return (
      <div className="popup--create-gathering">
        <PopupClose />
        <div className="popup__content">
          <div>Enter gathering title:</div>
          <input className="form-block" type="text" ref="title" placeholder="Gathering title " />
          <div>Hit OK when done, or click Cancel if you changed your mind.</div>
          <div>{this.state.msg}</div>
        </div>
        <PopupButtons />
      </div>
    )
  }
}

CreateGathering.contextTypes = {
  router: PropTypes.object.isRequired
}

CreateGathering.propTypes = {
  setPopupButtons: PropTypes.func.isRequired,
  closePopup: PropTypes.func.isRequired,
  createGathering: PropTypes.func.isRequired
}

const mapDispatchToProps = dispatch => {
  return {
    setPopupButtons: (buttons) => {
      dispatch(setPopupButtons(buttons))
    },
    closePopup: () => {
      dispatch(closePopup())
    },
    createGathering: (title) => {
      return createGathering(title)
    }
  }
}

export default connect(null, mapDispatchToProps)(CreateGathering)
