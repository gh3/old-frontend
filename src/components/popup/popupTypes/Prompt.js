import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PopupButtons from '../PopupButtons'
import PopupClose from '../PopupClose'
import { connect } from 'react-redux'

class Prompt extends Component {

  render() {
    return (
      <div className="popup--prompt">
        <PopupClose />
          <div className="popup__content">
            <div>{this.props.data.text}</div>
            <div>{this.props.data.msg}</div>
          </div>
        <PopupButtons />
      </div>
    )
  }
}

Prompt.propTypes = {
  data: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
  return {
    data: store.popupStore.data
  }
}

export default connect(mapStoreToProps)(Prompt)
