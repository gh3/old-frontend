import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import PopupButtons from '../PopupButtons'
import PopupClose from '../PopupClose'
import { setPopupButtons, closePopup } from '../../../actions/Popup'
import { photoNameUpdate, photoNameUpdated } from '../../../actions/Photos'

class EditPhotoName extends Component {

  constructor() {
    super()
    this.state = {
      msg: ''
    }

    this.handleCancel = this.handleCancel.bind(this)
    this.updatePhotoName = this.updatePhotoName.bind(this)
  }

  componentWillMount() {
    this.props.setPopupButtons([
      {
        order: 1,
        caption: 'Save',
        callback: this.updatePhotoName
      }, {
        order: 2,
        caption: 'Cancel',
        callback: this.handleCancel
      }
    ])
  }

  componentDidMount() {
    this.refs.name.select()
  }

  handleCancel() {
    this.props.closePopup()
  }

  updatePhotoName() {
    if (!this.refs.name.value) {
      this.setState({
        msg: 'Please enter photo name!'
      })
      return
    }

    if (this.refs.name.value === this.props.data.photoName) {
      this.setState({
        msg: 'New name is the same as old one!'
      })
      return
    }

    this.setState({
      msg: ''
    })

    this.props.photoNameUpdate(this.refs.name.value, this.props.data.gatheringPhotoId).then(rsp => {
      if (rsp.status === 'ok') {
        this.props.photoNameUpdated(this.refs.name.value, this.props.data.gatheringPhotoId)
        this.props.closePopup()
      } else if (rsp.status === 'error') {
        this.setState({
          msg: rsp.msg
        })
      }
    })
  }

  render() {
    return (
      <div className="popup--edit-photo-name">
        <PopupClose />
        <div className="popup__content">
          <div>Custom photo name:</div>
          <input className="form-block" type="text" ref="name" defaultValue={this.props.data.photoName} />
          <div>{this.state.msg}</div>
        </div>
        <PopupButtons />
      </div>
    )
  }
}

EditPhotoName.propTypes = {
  data: PropTypes.object.isRequired,
  setPopupButtons: PropTypes.func.isRequired,
  closePopup: PropTypes.func.isRequired,
  photoNameUpdate: PropTypes.func.isRequired,
  photoNameUpdated: PropTypes.func.isRequired
}

const mapStoreToProps = function(store) {
  return {
    data: store.popupStore.data
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setPopupButtons: (buttons) => {
      dispatch(setPopupButtons(buttons))
    },
    closePopup: () => {
      dispatch(closePopup())
    },
    photoNameUpdate: (name, gatheringPhotoId) => {
      return photoNameUpdate(name, gatheringPhotoId)
    },
    photoNameUpdated: (name, gatheringPhotoId) => {
      dispatch(photoNameUpdated(name, gatheringPhotoId))
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(EditPhotoName)
