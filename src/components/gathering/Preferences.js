import React, { Component } from 'react'
import { connect } from 'react-redux'

class Preferences extends Component {

  render() {
    return (
      <div>
        <strong>Preferences...</strong>
      </div>
    )
  }
}

const mapStoreToProps = function(store) {
  return {
    gathering: store.gatheringStore.gathering
  }
}

export default connect(mapStoreToProps)(Preferences)