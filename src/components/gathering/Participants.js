import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { findParticipantsByGathering, removeOneParticipant } from '../../actions/Participants'

class Participants extends Component {

  constructor() {
    super()

    this.handleRemoveParticipant = this.handleRemoveParticipant.bind(this)
    this.getGatheringParticipants = this.getGatheringParticipants.bind(this)
  }

  componentWillMount() {
    if (this.props.match.params.gatheringId) {
      this.getGatheringParticipants(this.props.match.params.gatheringId)
    }
  }

  getGatheringParticipants(gatheringId) {
    this.props.findParticipantsByGathering(gatheringId)
  }

  handleRemoveParticipant(userId) {
    this.props.removeOneParticipant(userId, this.props.gathering._id)
  }

  render() {
    return (
      <div>
        <strong>Participants:</strong>
        <div>
          {this.props.gatheringParticipants.map(function(gatheringParticipant) {
            if (gatheringParticipant._id === this.props.whoami._id) {
              return (
                <div key={gatheringParticipant._id}>
                  <span>{gatheringParticipant.name ? gatheringParticipant.name : 'Unknown'}</span>
                </div>
              )
            }

            return (
              <div key={gatheringParticipant._id}>
                <a onClick={this.handleRemoveParticipant.bind(this, gatheringParticipant._id)}>X</a> <span>{gatheringParticipant.name ? gatheringParticipant.name : 'Unknown'}</span>
              </div>
            )
          }.bind(this))}
        </div>
      </div>
    )
  }
}

Participants.propTypes = {
  gathering: PropTypes.object.isRequired,
  gatheringParticipants: PropTypes.array.isRequired,
  whoami: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
  return {
    gathering: store.gatheringStore.gathering,
    gatheringParticipants: store.gatheringStore.gatheringParticipants,
    whoami: store.userStore.whoami
  }
}

const mapDispatchToProps = dispatch => {
  return {
    findParticipantsByGathering: (gatheringId) => {
      dispatch(findParticipantsByGathering(gatheringId))
    },
    removeOneParticipant: (userId, gatheringId) => {
      dispatch(removeOneParticipant(userId, gatheringId))
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(Participants)
