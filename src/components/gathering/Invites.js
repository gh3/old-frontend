import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { PopupTypes } from '../../Constants'
import { openPopup } from '../../actions/Popup'
import { findInvitesByGathering, removeOneGatheringInvite } from '../../actions/Invites'

class Invites extends Component {

  constructor() {
    super()

    this.handleGatheringInvite = this.handleGatheringInvite.bind(this)
    this.handleRemoveInvite = this.handleRemoveInvite.bind(this)
    this.getGatheringInvites = this.getGatheringInvites.bind(this)
  }

  componentWillMount() {
    if (this.props.match.params.gatheringId) {
      this.getGatheringInvites(this.props.match.params.gatheringId)
    }
  }

  getGatheringInvites(gatheringId) {
    this.props.findInvitesByGathering(gatheringId)
  }

  handleGatheringInvite() {
    const buttons = []

    const popupData = {
      gatheringId: this.props.gathering._id
    }

    this.props.openPopup(PopupTypes.GATHERING_INVITE, buttons, popupData)
  }

  handleRemoveInvite(gatheringInviteId) {
    this.props.removeOneGatheringInvite(gatheringInviteId, this.props.gathering._id)
  }

  render() {
    return (
      <div>
        <strong>Invites:</strong>
        <div>
          {this.props.gatheringInvites.map(function(gatheringInvite) {
            return (
              <div key={gatheringInvite._id}>
                <a onClick={this.handleRemoveInvite.bind(this, gatheringInvite._id)}>X</a> <span>{gatheringInvite.phone}</span> <strong>{gatheringInvite.respond}</strong>
              </div>
            )
          }.bind(this))}
        </div>
        <div>
          <span>Want to add someone to the gathering?</span> <a onClick={this.handleGatheringInvite} style={{lineHeight: '50px'}}>Invite here!</a>
        </div>
      </div>
    )
  }
}

Invites.propTypes = {
  gathering: PropTypes.object.isRequired,
  gatheringInvites: PropTypes.array.isRequired
}

const mapStoreToProps = function(store) {
  return {
    gathering: store.gatheringStore.gathering,
    gatheringInvites: store.gatheringStore.gatheringInvites
  }
}

const mapDispatchToProps = dispatch => {
  return {
    openPopup: (popupType, buttons, data) => {
      dispatch(openPopup(popupType, buttons, data))
    },
    findInvitesByGathering: (gatheringId) => {
      dispatch(findInvitesByGathering(gatheringId))
    },
    removeOneGatheringInvite: (gatheringInviteId, gatheringId) => {
      dispatch(removeOneGatheringInvite(gatheringInviteId, gatheringId))
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(Invites)
