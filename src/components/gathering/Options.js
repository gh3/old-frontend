import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import LeaveGathering from './options/LeaveGathering'
import RemoveGathering from './options/RemoveGathering'

class Options extends Component {

  renderLeaderButtons() {
    return (
      <div>
        <div><Link to={'/gathering/' + this.props.gathering._id + '/participants'}>Participants</Link></div>
        <div><Link to={'/gathering/' + this.props.gathering._id + '/invites'}>Invites</Link></div>
        <div><RemoveGathering /></div>
      </div>
    )
  }

  renderParticipantButtons() {
    return (
      <div>
        <div><LeaveGathering /></div>
      </div>
    )
  }

  render() {
    let buttonsList
    if (this.props.gathering.leader) {
      buttonsList = this.renderLeaderButtons()
    } else {
      buttonsList = this.renderParticipantButtons()
    }

    return (
      <div>
        <strong>Options:</strong>
        {buttonsList}
      </div>
    )
  }
}

Options.propTypes = {
  gathering: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
  return {
    gathering: store.gatheringStore.gathering
  }
}

export default connect(mapStoreToProps)(Options)
