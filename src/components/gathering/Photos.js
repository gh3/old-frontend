import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PhotoUploader from './photos/PhotoUploader'
import { PopupTypes } from '../../Constants'
import { connect } from 'react-redux'
import { findIndex } from 'lodash'
import { findParticipantsByGathering } from '../../actions/Participants'
import { findPhotosByGathering, removePhoto, removePhotoAsLeader } from '../../actions/Photos'
import { openPopup, closePopup } from '../../actions/Popup'
import { fetchPhoto, arrayBufferToBase64 } from '../../Utils'

class Photos extends Component {

  constructor() {
    super()

    this.state = {
      gatheringPhotos: []
    }

    this.handleEditPhotoName = this.handleEditPhotoName.bind(this)
    this.handleRemovePhotoToTrash = this.handleRemovePhotoToTrash.bind(this)
    this.removePhotoToTrash = this.removePhotoToTrash.bind(this)
    this.renderPhotoName = this.renderPhotoName.bind(this)
    this.renderUserName = this.renderUserName.bind(this)
    this.renderRemoveBtn = this.renderRemoveBtn.bind(this)
    this.getGatheringPhotos = this.getGatheringPhotos.bind(this)
    this.requestImageSource = this.requestImageSource.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
  }

  componentWillMount() {
    if (this.props.match.params.gatheringId) {
      this.getGatheringPhotos(this.props.match.params.gatheringId)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.gatheringPhotos !== nextProps.gatheringPhotos) {

      this.setState({
        gatheringPhotos: nextProps.gatheringPhotos
      })

      nextProps.gatheringPhotos.forEach(gatheringPhoto => {
        this.requestImageSource(gatheringPhoto)
      })
    }
  }

  requestImageSource(gatheringPhoto) {
    // https://medium.com/front-end-hacking/fetching-images-with-the-fetch-api-fb8761ed27b2
    const photoCropURL = '/photo/' + gatheringPhoto.guid + '_300x300.jpg' 

    fetchPhoto(photoCropURL, {}).then(response => {
      response.arrayBuffer().then((buffer) => {
        let base64Flag = 'data:' + gatheringPhoto.type + ';base64,'
        let imageStr = arrayBufferToBase64(buffer)
        gatheringPhoto.src = base64Flag + imageStr

        let gatheringPhotos = this.state.gatheringPhotos
        let gatheringPhotoIndex = findIndex(gatheringPhotos, { _id: gatheringPhoto._id })
        gatheringPhotos[gatheringPhotoIndex] = gatheringPhoto

        this.setState({
          gatheringPhotos: gatheringPhotos
        })
      })
    })
  }

  getGatheringPhotos(gatheringId) {
    this.props.findPhotosByGathering(gatheringId)
    this.props.findParticipantsByGathering(gatheringId)
  }

  handleEditPhotoName(gatheringPhotoId, photoName) {
    const buttons = []

    const popupData = {
      gatheringPhotoId: gatheringPhotoId,
      photoName: photoName
    }

    this.props.openPopup(PopupTypes.EDIT_PHOTO_NAME, buttons, popupData)
  }

  handleRemovePhotoToTrash(gatheringPhoto) {
    const buttons = [
      {
        order: 1,
        caption: 'Yes',
        callback: this.removePhotoToTrash
      }, {
        order: 2,
        caption: 'Cancel',
        callback: this.handleCancel
      }
    ]

    const popupData = {
      text: 'Are you sure you want to remove this photo?',
      gatheringPhoto: gatheringPhoto
    }

    this.props.openPopup(PopupTypes.PROMPT, buttons, popupData)
  }

  handleCancel() {
    this.props.closePopup()
  }

  removePhotoToTrash() {
    let isGatheringLeader = this.props.gathering.userId === this.props.whoami._id ? true : false
    let isPhotoOwner = this.props.popupData.gatheringPhoto.userId === this.props.whoami._id ? true : false

    if (isPhotoOwner) {
      this.props.removePhoto(this.props.popupData.gatheringPhoto._id)
    } else if (isGatheringLeader) {
      this.props.removePhoto(this.props.popupData.gatheringPhoto._id, this.props.gathering._id)
    } else {
      // Not photo owner or gathering leader, no fun!
    }
  }

  renderPhotoName(gatheringPhoto) {
    let photoName = gatheringPhoto.customName ? gatheringPhoto.customName : gatheringPhoto.name
    let isPhotoOwner = gatheringPhoto.userId === this.props.whoami._id ? true : false

    if (isPhotoOwner) {
      return (
        <a className="gathering-photo-name" onClick={this.handleEditPhotoName.bind(this, gatheringPhoto._id, photoName)}>
          <strong>{photoName}</strong>
        </a>
      )
    } else {
      return (
        <strong className="gathering-photo-name">{photoName}</strong>
      )
    }
  }

  renderUserName(gatheringPhoto) {
    if (!this.props.gatheringParticipants.length) {
      return null
    }

    let i = this.props.gatheringParticipants.findIndex(elm => elm._id === gatheringPhoto.userId)
    let userName = i > -1 && this.props.gatheringParticipants[i].name ? this.props.gatheringParticipants[i].name : 'Unknown'

    return (
      <span>{userName}: </span>
    )
  }

  renderRemoveBtn(gatheringPhoto) {
    let isGatheringLeader = this.props.gathering.userId === this.props.whoami._id ? true : false
    let isPhotoOwner = gatheringPhoto.userId === this.props.whoami._id ? true : false

    if (isGatheringLeader || isPhotoOwner) {
      return (
        <a onClick={this.handleRemovePhotoToTrash.bind(this, gatheringPhoto)}>X</a>
      )
    }

    return null
  }

  render() {
    return (
      <div>
        <PhotoUploader />
        <div>
          {this.state.gatheringPhotos.map(function(gatheringPhoto) {
            return (
              <div key={gatheringPhoto._id}>
                <img src={gatheringPhoto.src} className="gathering-photo" alt="" />
                {this.renderUserName(gatheringPhoto)} {this.renderPhotoName(gatheringPhoto)} {this.renderRemoveBtn(gatheringPhoto)}
              </div>
            )
          }.bind(this))}
        </div>
      </div>
    )
  }
}

Photos.propTypes = {
  gathering: PropTypes.object.isRequired,
  gatheringPhotos: PropTypes.array.isRequired,
  gatheringParticipants: PropTypes.array.isRequired,
  whoami: PropTypes.object.isRequired,
  popupData: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
  return {
    gathering: store.gatheringStore.gathering,
    gatheringPhotos: store.gatheringStore.gatheringPhotos,
    gatheringParticipants: store.gatheringStore.gatheringParticipants,
    whoami: store.userStore.whoami,
    popupData: store.popupStore.data
  }
}

const mapDispatchToProps = dispatch => {
  return {
    openPopup: (popupType, buttons, data) => {
      dispatch(openPopup(popupType, buttons, data))
    },
    closePopup: () => {
      dispatch(closePopup())
    },
    findParticipantsByGathering: (gatheringId) => {
      dispatch(findParticipantsByGathering(gatheringId))
    },
    findPhotosByGathering: (gatheringId) => {
      dispatch(findPhotosByGathering(gatheringId))
    },
    removePhoto: (gatheringPhotoId) => {
      dispatch(removePhoto(gatheringPhotoId))
    },
    removePhotoAsLeader: (gatheringPhotoId, gatheringId) => {
      dispatch(removePhotoAsLeader(gatheringPhotoId, gatheringId))
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(Photos)
