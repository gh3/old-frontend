import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { b64toBlob } from '../../../Utils'

class PhotoShooter extends Component {
  componentDidMount() {
    const handleFile = this.props.handleFile
    /* eslint-disable no-undef */
    var app = {
      startCamera: function(){
        CameraPreview.startCamera({x: 50, y: 50, width: 300, height: 300, toBack: false, previewDrag: true, tapPhoto: true});
      },

      stopCamera: function(){
        CameraPreview.stopCamera();
      },

      takePicture: function(){
        CameraPreview.takePicture(function(imgData){
          const timestamp = new Date().getTime()
          const fileData = b64toBlob(imgData, 'image/jpeg')
          const capturedPhoto = new File([fileData], 'GH3_' + timestamp + '.jpg')
          handleFile(capturedPhoto)
          app.stopCamera()
        });
      },

      switchCamera: function(){
        CameraPreview.switchCamera();
      },

      show: function(){
        CameraPreview.show();
      },

      hide: function(){
        CameraPreview.hide();
      },

      changeColorEffect: function(){
        var effect = document.getElementById('selectColorEffect').value;
        CameraPreview.setColorEffect(effect);
      },

      changeFlashMode: function(){
        var mode = document.getElementById('selectFlashMode').value;
        CameraPreview.setFlashMode(mode);
      },

      showSupportedPictureSizes: function(){
        CameraPreview.getSupportedPictureSizes(function(dimensions){
          let pictureSizes = ''
          dimensions.forEach(function(dimension) {
            pictureSizes += dimension.width + 'x' + dimension.height + '\n'
          });
          alert(pictureSizes)
        });
      },

      init: function(){
        document.getElementById('startCameraButton').addEventListener('click', this.startCamera, false);
        document.getElementById('stopCameraButton').addEventListener('click', this.stopCamera, false);
        document.getElementById('switchCameraButton').addEventListener('click', this.switchCamera, false);
        document.getElementById('showButton').addEventListener('click', this.show, false);
        document.getElementById('hideButton').addEventListener('click', this.hide, false);
        document.getElementById('takePictureButton').addEventListener('click', this.takePicture, false);
        document.getElementById('selectColorEffect').addEventListener('change', this.changeColorEffect, false);
        document.getElementById('selectFlashMode').addEventListener('change', this.changeFlashMode, false);
        document.getElementById('showSupportedPictureSizes').addEventListener('click', this.showSupportedPictureSizes, false);
        window.addEventListener('orientationchange', () => {
          this.stopCamera()
          this.startCamera()
        }, false);
      }
    }

    app.init()
    /* eslint-enable no-undef */
  }

  render() {
    return (
      <div>
        <div className="controls">
          <div className="block">
            <button id="startCameraButton">Start Camera</button>
            <button id="stopCameraButton">Stop Camera</button>
          </div>
          <div className="block">
            <p>
              Color Effect:
              <select id="selectColorEffect">
                <option value="none">none</option>
                <option value="aqua">aqua</option>
                <option value="blackboard">blackboard</option>
                <option value="mono">mono</option>
                <option value="negative">negative</option>
                <option value="posterize">posterize</option>
                <option value="sepia">sepia</option>
                <option value="solarize">solarize</option>
                <option value="whiteboard">whiteboard</option>
              </select>
            </p>
            <p>
              Flash Mode:
              <select id="selectFlashMode">
                <option value="off">off</option>
                <option value="on">on</option>
                <option value="auto">auto</option>
                <option value="torch">torch</option>
              </select>
            </p>
          </div>
          <div className="block">
            <button id="takePictureButton">Take Picture</button>
            <button id="switchCameraButton">Switch Camera</button>
          </div>
          <div className="block">
            <button id="hideButton">Hide</button>
            <button id="showButton">Show</button>
          </div>
          <div className="block">
            <button id="showSupportedPictureSizes">Supported Picture Sizes</button>
          </div>
        </div>
      </div>
    )
  }
}

PhotoShooter.propTypes = {
  gathering: PropTypes.object.isRequired,
  handleFile: PropTypes.func.isRequired
}

const mapStoreToProps = function(store) {
  return {
    gathering: store.gatheringStore.gathering
  }
}

const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStoreToProps, mapDispatchToProps)(PhotoShooter)
