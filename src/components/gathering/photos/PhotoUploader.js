import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { generateGuid } from '../../../Utils'
import { publishPhoto, photoInserted } from '../../../actions/Photos'
import { startCamera } from '../../../actions/CameraPreview'

let xhrs = {}

class PhotoUploader extends Component {
  constructor() {
    super()
    this.state = {
      addedFiles: [],
      uploadProgress: {},
      newGatheringPhotos: {}
    }

    this.triggerFileInput = this.triggerFileInput.bind(this)
    this.handleFileInput = this.handleFileInput.bind(this)
    this.handleDragEnter = this.handleDragEnter.bind(this)
    this.handleDragOver = this.handleDragOver.bind(this)
    this.handleDrop = this.handleDrop.bind(this)
    this.handleFiles = this.handleFiles.bind(this)
    this.handleFile = this.handleFile.bind(this)
    this.getOrientation = this.getOrientation.bind(this)
    this.presentPhoto = this.presentPhoto.bind(this)
    this.uploadPhoto = this.uploadPhoto.bind(this)
    this.updateUploadProgress = this.updateUploadProgress.bind(this)
    this.handleRemovePhoto = this.handleRemovePhoto.bind(this)
    this.handlePublish = this.handlePublish.bind(this)
    this.renderPublishButton = this.renderPublishButton.bind(this)
    this.renderPhotoPreviewThumb = this.renderPhotoPreviewThumb.bind(this)
    this.renderPhotoPreview = this.renderPhotoPreview.bind(this)
  }

  triggerFileInput() {
    document.getElementById('file-input').click()
  }

  handleFileInput() {
    let files = document.getElementById('file-input').files
    this.handleFiles(files)
  }

  handleDragEnter(e) {
    e.stopPropagation()
    e.preventDefault()
  }

  handleDragOver(e) {
    e.stopPropagation()
    e.preventDefault()
  }

  handleDrop(e) {
    e.stopPropagation()
    e.preventDefault()
    let files = e.dataTransfer.files
    this.handleFiles(files)
  }

  handleFiles(files) {
    for (let i = 0; i < files.length; i++) {
      this.handleFile(files[i])
    }
  }

  handleFile(file) {
    file.gatheringId = this.props.gathering._id
    try {
      file.src = window.URL.createObjectURL(file)
      file.key = file.src.split('/').pop(-1)            
      this.getOrientation(file, this.presentPhoto)
    } catch (e) {
      // Browser does not support createObjectURL!
      file.key = generateGuid()
      this.presentPhoto(file)
    }
  }

  getOrientation(file, callback) {
    // http://stackoverflow.com/questions/7584794/accessing-jpeg-exif-rotation-data-in-javascript-on-the-client-side
    let reader = new FileReader()
    reader.onload = function(e) {
      let view = new DataView(e.target.result)
      if (view.getUint16(0, false) !== 0xFFD8) {
        file.orientation = -2
        return callback(file)
      }
      let length = view.byteLength
      let offset = 2
      while (offset < length) {
        let marker = view.getUint16(offset, false)
        offset += 2
        if (marker === 0xFFE1) {
          let known = view.getUint32(offset += 2, false) !== 0x45786966
          if (known) {
            file.orientation = -2
            return callback(file)
          }
          let little = view.getUint16(offset += 6, false) === 0x4949
          offset += view.getUint32(offset + 4, little)
          let tags = view.getUint16(offset, little)
          offset += 2
          for (let i = 0; i < tags; i++) {
            if (view.getUint16(offset + (i * 12), little) === 0x0112) {
              file.orientation = view.getUint16(offset + (i * 12) + 8, little)
              return callback(file)
            }
          }
        } else if ((marker & 0xFF00) !== 0xFF00) {
          break
        } else {
          offset += view.getUint16(offset, false)
        }
      }
      file.orientation = -1
      return callback(file)
    }
    reader.readAsArrayBuffer(file)
  }

  presentPhoto(file) {
    let addedFiles = this.state.addedFiles
    addedFiles.unshift(file)
    this.uploadPhoto(file)
    this.setState({
      addedFiles: addedFiles
    })
  }

  uploadPhoto(file) {
    let form = new FormData()
    form.append('gatheringId', this.props.gathering._id)
    form.append(file.key, file)

    let xhr = new XMLHttpRequest()
    xhrs[file.key] = xhr

    xhr.open('POST', process.env.REACT_APP_BASE_URL + '/api/gathering-photos/insert.json')
    xhr.setRequestHeader('Session', localStorage.getItem('session'));

    xhr.onreadystatechange = function() {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        let newGatheringPhotos = this.state.newGatheringPhotos

        let rsp = xhr.responseText ? JSON.parse(xhr.responseText) : {'status': 'error'}

        if (rsp.status === 'ok') {
          newGatheringPhotos[rsp.data.gatheringPhoto.fileKey] = rsp.data.gatheringPhoto
        } else {
          newGatheringPhotos[file.key] = false    
        }

        this.setState({
          newGatheringPhotos: newGatheringPhotos
        })
      }
    }.bind(this)

    xhr.upload.addEventListener('progress', function(e) {
      this.updateUploadProgress(e, file)
    }.bind(this))

    xhr.addEventListener('error', function(e) {
      this.updateUploadProgress(e, file)
    }.bind(this))

    xhr.send(form)
  }

  updateUploadProgress(e, file) {
    let uploadProgress = this.state.uploadProgress
    if (e.type === 'error') {
      uploadProgress[file.key] = 'error'
    } else {
      let progressNew = e.lengthComputable ? Math.round((e.loaded / e.total) * 100) : 0
      uploadProgress[file.key] = progressNew
    }
    this.setState({
      uploadProgress: uploadProgress
    })
  }

  handleRemovePhoto(i) {
    let addedFiles = this.state.addedFiles

    window.URL.revokeObjectURL(addedFiles[i].src)
    xhrs[addedFiles[i].key].abort()
    addedFiles.splice(i, 1)

    this.setState({
      addedFiles: addedFiles
    })
  }

  handlePublish() {
    let addedFiles = this.state.addedFiles.reverse()
    let newGatheringPhotos = this.state.newGatheringPhotos

    let i
    let addedFile
    let gatheringPhotoId

    for (i in addedFiles) {
      addedFile = addedFiles[i]
      gatheringPhotoId = newGatheringPhotos[addedFile.key]._id

      this.props.publishPhoto(gatheringPhotoId).then(rsp => {
        if (rsp.status === 'ok') {
          let addedFiles = this.state.addedFiles
          let i = addedFiles.findIndex(elm => elm.key === rsp.data.gatheringPhotoKey)

          addedFiles.splice(i, 1)

          this.setState({
            addedFiles: addedFiles
          })

          this.props.photoInserted(rsp.data.gatheringPhoto)
        }
      })
    }
  }

  renderPublishButton() {
    let addedFiles = this.state.addedFiles

    if (!addedFiles.length) {
      return null
    }

    let uploadProgress = this.state.uploadProgress
    let newGatheringPhotos = this.state.newGatheringPhotos

    let notUploadedYetCount = 0
    let notSavedYetCount = 0

    let i
    let addedFile

    for (i in addedFiles) {
      addedFile = addedFiles[i]

      if (uploadProgress[addedFile.key] !== 100) {
        notUploadedYetCount++
      }

      if (!newGatheringPhotos[addedFile.key]) {
        notSavedYetCount++
      }
    }

    if (notUploadedYetCount) {
      return null
    }

    if (notSavedYetCount) {
      return null
    }

    return (
      <a onClick={this.handlePublish} className="pretty-btn">PUBLISH!!</a>
    )
  }

  renderPhotoPreviewThumb(upload) {
    let className = 'photo-uploader__preview'
    if (upload.orientation && upload.orientation > 0) {
      className += ' orientation-' + upload.orientation
    }
    return (
      <div className={className} style={{backgroundImage: 'url(' + upload.src + ')'}}></div>
    )
  }

  renderPhotoPreview(upload, i) {
    return (
      <div className="photo-uploader__added-file" key={upload.key}>
        {upload.hasOwnProperty('src') ? this.renderPhotoPreviewThumb(upload) : null}
        <div>
          <span>{upload.name}</span>
          <strong> {this.state.uploadProgress[upload.key] === 'error' ? 'error' : this.state.uploadProgress[upload.key]}</strong>
          <i> {this.state.newGatheringPhotos[upload.key] ? this.state.newGatheringPhotos[upload.key]._id : null}</i>
        </div>
        <a onClick={this.handleRemovePhoto.bind(this, i)}>X</a>
      </div>
    )
  }

  renderPhotoShooter() {
    if (!window.cordova) {
      return null
    }

    return (
      <div className="photo-uploader__photo-shooter" onClick={this.props.startCamera}>
        <span>Take a Photo</span>
      </div>
    )
  }

  render() {
    if (typeof FormData === 'undefined') {
      return (
        <div className="photo-uploader">Photo uploader is not supported on older devices. Please upgrade yourself!</div>
      )
    }

    return (
      <div className="photo-uploader">
        {this.renderPhotoShooter()}
        <input type="file" id="file-input" multiple="multiple" accept="image/*" onChange={this.handleFileInput} style={{display: 'none'}}/>
        <div className="photo-uploader__upload-area" onClick={this.triggerFileInput} onDragEnter={this.handleDragEnter} onDragOver={this.handleDragOver} onDrop={this.handleDrop}>
          <span>Upload Photos</span>
        </div>
        <div className="photo-uploader__added-files">
          {this.state.addedFiles.map(this.renderPhotoPreview)}
        </div>
        {this.renderPublishButton()}
      </div>
    )
  }
}

PhotoUploader.propTypes = {
  gathering: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
  return {
    gathering: store.gatheringStore.gathering
  }
}

const mapDispatchToProps = dispatch => {
  return {
    publishPhoto: (gatheringPhotoId) => {
      return publishPhoto(gatheringPhotoId)
    },
    photoInserted: (gatheringPhoto) => {
      dispatch(photoInserted(gatheringPhoto))
    },
    startCamera: () => {
      dispatch(startCamera())
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(PhotoUploader)
