import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { PopupTypes } from '../../../Constants'
import { openPopup, closePopup, setPopupData, setPopupButtons } from '../../../actions/Popup'
import { prepareForRemoving, gatheringRemovingSet, cancelGatheringRemoving } from '../../../actions/Gatherings'

class RemoveGathering extends Component {
  constructor() {
    super()

    this.handleCancel = this.handleCancel.bind(this)
    this.handleRemove = this.handleRemove.bind(this)
    this.removeGathering = this.removeGathering.bind(this)
    this.handleRemovingCancelation = this.handleRemovingCancelation.bind(this)
  }

  handleRemove() {
    const buttons = [
      {
        order: 1,
        caption: 'Yes',
        callback: this.removeGathering
      }, {
        order: 2,
        caption: 'Cancel',
        callback: this.handleCancel
      }
    ]

    const popupData = {
      text: 'Are you sure you want to remove ' + this.props.gathering.title + '? '
    }

    this.props.openPopup(PopupTypes.PROMPT, buttons, popupData)
  }

  handleCancel() {
    this.props.closePopup()
  }

  removeGathering() {
    this.props.prepareForRemoving(this.props.gathering._id).then(rsp => {
      if (rsp.status === 'ok') {
        this.props.gatheringRemovingSet(this.props.gathering._id, rsp.data.removingDate)

        this.props.setPopupData({
          text: rsp.msg,
          msg: ''
        })

        this.props.setPopupButtons([{
          order: 1,
          caption: 'Close',
          callback: this.handleCancel
        }])

      } else {
        this.props.setPopupData({
          text: 'Are you sure you want to remove ' + this.props.gathering.title + '?',
          msg: rsp.msg
        })
      }
    })
  }

  handleRemovingCancelation() {
    this.props.cancelGatheringRemoving(this.props.gathering._id)
  }

  render() {
    if (this.props.gathering.removingDate) {
      return (
        <a onClick={this.handleRemovingCancelation}>Cancel removing</a>
      )
    }

    return (
      <a onClick={this.handleRemove}>Remove it</a>
    )
  }
}

RemoveGathering.propTypes = {
  gathering: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
  return {
    gathering: store.gatheringStore.gathering
  }
}

const mapDispatchToProps = dispatch => {
  return {
    openPopup: (popupType, buttons, data) => {
      dispatch(openPopup(popupType, buttons, data))
    },
    setPopupData: (data) => {
      dispatch(setPopupData(data))
    },
    setPopupButtons: (data) => {
      dispatch(setPopupButtons(data))
    },
    closePopup: () => {
      dispatch(closePopup())
    },
    prepareForRemoving: (gatheringId) => {
      return prepareForRemoving(gatheringId)
    },
    gatheringRemovingSet: (gatheringId, removingDate) => {
      dispatch(gatheringRemovingSet(gatheringId, removingDate))
    },
    cancelGatheringRemoving: (gatheringId) => {
      dispatch(cancelGatheringRemoving(gatheringId))
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(RemoveGathering)
