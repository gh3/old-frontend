import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { PopupTypes } from '../../../Constants'
import { openPopup, closePopup, setPopupData } from '../../../actions/Popup'
import { removeMeAsParticpant } from '../../../actions/Participants'
import { removedOneGathering } from '../../../actions/Gatherings'

class LeaveGathering extends Component {
  constructor() {
    super()

    this.handleCancel = this.handleCancel.bind(this)
    this.handleLeave = this.handleLeave.bind(this)
    this.removeMeFromGathering = this.removeMeFromGathering.bind(this)
  }

  handleLeave() {
    const buttons = [
      {
        order: 1,
        caption: 'Yes',
        callback: this.removeMeFromGathering
      }, {
        order: 2,
        caption: 'Cancel',
        callback: this.handleCancel
      }
    ]

    const popupData = {
      text: 'Are you sure you want to leave ' + this.props.gathering.title + '?'
    }

    this.props.openPopup(PopupTypes.PROMPT, buttons, popupData)
  }

  handleCancel() {
    this.props.closePopup()
  }

  removeMeFromGathering() {
    this.props.removeMeAsParticpant(this.props.gathering._id).then(rsp => {
      if (rsp.status === 'ok') {
        this.props.removedOneGathering(this.props.gathering._id)
        this.props.closePopup()
        this.context.router.history.push('/app')
      } else {
        this.props.setPopupData({
          text: 'Are you sure you want to leave ' + this.props.gathering.title + '?',
          msg: rsp.msg
        })
      }
    })
  }

  render() {
    return (
      <a onClick={this.handleLeave}>Leave it</a>
    )
  }
}

LeaveGathering.contextTypes = {
  router: PropTypes.object.isRequired
}

LeaveGathering.propTypes = {
  gathering: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
  return {
    gathering: store.gatheringStore.gathering
  }
}

const mapDispatchToProps = dispatch => {
  return {
    openPopup: (popupType, buttons, data) => {
      dispatch(openPopup(popupType, buttons, data))
    },
    setPopupData: (data) => {
      dispatch(setPopupData(data))
    },
    closePopup: () => {
      dispatch(closePopup())
    },
    removeMeAsParticpant: (gatheringId) => {
      return removeMeAsParticpant(gatheringId)
    },
    removedOneGathering: (gatheringId) => {
      dispatch(removedOneGathering(gatheringId))
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(LeaveGathering)
