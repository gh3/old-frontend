import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { setSession } from '../../actions/Auth'
import { checkBelonging, loginViaInvite, registerViaInvite, respondOnInvite } from '../../actions/Invites'
import { isEmpty, isEqual } from 'lodash'
import { GatheringInviteModes, GatheringInviteRespond } from '../../Constants'

class GatheringInvite extends Component {

  constructor() {
    super()
    this.state = {
      mode: GatheringInviteModes.INITIALIZATION,
      msg: 'Processing token in progress...',
      gatheringInvite: false,
      gathering: false,
      respond: ''
    }

    this.registerWithToken = this.registerWithToken.bind(this)
    this.loginWithToken = this.loginWithToken.bind(this)
    this.respondOnToken = this.respondOnToken.bind(this)
    this.handleContinueToApp = this.handleContinueToApp.bind(this)
  }

  componentWillMount() {
    if (!isEmpty(this.props.areacodes)) {
      this.checkInviteToken()
    }
  }

  componentDidUpdate(prevProps) {
    if (!isEqual(prevProps.areacodes, this.props.areacodes)) {
      this.checkInviteToken()
    }
  }

  checkInviteToken() {
    this.setState({
      mode: GatheringInviteModes.CHECK_TOKEN_STARTED
    })

    this.props.checkBelonging(this.props.match.params.token).then(rsp => {
      this.setState({
        mode: GatheringInviteModes.CHECK_TOKEN_RESPONSE
      })
      if (rsp.status === 'ok') {
        this.setState({
          gatheringInvite: rsp.data.gatheringInvite,
          gathering: rsp.data.gathering
        })
        if (rsp.data.user) {
          this.loginWithToken(rsp.data.gatheringInvite)
        } else {
          this.registerWithToken(rsp.data.gatheringInvite)
        }
      } else {
        this.setState({
          msg: rsp.msg
        })
      }
    })
  }

  registerWithToken(gatheringInvite) {
    const inviteAreacode = this.props.areacodes.find(elm => elm._id === gatheringInvite.areacodeId)

    if (!inviteAreacode) {
      this.setState({
        mode: GatheringInviteModes.AREACODE_NOT_FOUND,
        msg: 'Error happened while processing token.'
      })
    }

    this.props.registerViaInvite(inviteAreacode.areacode, gatheringInvite.phone, this.props.match.params.token).then(rsp => {      
      this.setState({
        mode: GatheringInviteModes.REGISTER_RESPONSE
      })
      if (rsp.status === 'ok') {
        this.props.setSession(rsp.data.session)
        this.respondOnToken(GatheringInviteRespond.ACCEPTED)
      } else {
        this.setState({
          msg: rsp.msg
        })
      }
    })
  }

  loginWithToken(gatheringInvite) {
    const inviteAreacode = this.props.areacodes.find(elm => elm._id === gatheringInvite.areacodeId)

    if (!inviteAreacode) {
      this.setState({
        mode: GatheringInviteModes.AREACODE_NOT_FOUND,
        msg: 'Error happened while processing token.'
      })
    }

    this.props.loginViaInvite(inviteAreacode.areacode, gatheringInvite.phone, this.props.match.params.token).then(rsp => {      
      this.setState({
        mode: GatheringInviteModes.LOGIN_RESPONSE
      })
      if (rsp.status === 'ok') {
        this.props.setSession(rsp.data.session)

        if (!gatheringInvite.respond) {
          this.setState({
            mode: GatheringInviteModes.ASK_FOR_TOKEN_RESPOND,
            msg: ''
          })
        } else if (gatheringInvite.respond === GatheringInviteRespond.REJECTED) {
          this.setState({
            msg: 'Gathering invite was already rejected!'
          })
        } else if (gatheringInvite.respond === GatheringInviteRespond.ACCEPTED) {
          this.context.router.history.push('/gathering/' + this.state.gatheringInvite.gatheringId)
        }

      } else {
        this.setState({
          msg: rsp.msg
        })
      }
    })
  }

  respondOnToken(respond) {
    this.props.respondOnInvite(this.props.match.params.token, respond).then(rsp => {
      this.setState({
        mode: GatheringInviteModes.TOKEN_RESPONSE
      })
      if (rsp.status === 'ok') {
        if (respond === GatheringInviteRespond.REJECTED) {
          this.setState({
            respond: GatheringInviteRespond.REJECTED,
            msg: rsp.msg
          })
        } else if (respond === GatheringInviteRespond.ACCEPTED) {
          this.context.router.history.push('/gathering/' + this.state.gatheringInvite.gatheringId)
        }
      } else {
        this.setState({
          msg: rsp.msg
        })
      }
    })
  }

  handleContinueToApp() {
    this.context.router.history.push('/app')
  }

  render() {
    if (this.state.mode === GatheringInviteModes.TOKEN_RESPONSE && this.state.respond === GatheringInviteRespond.REJECTED) {
      return (
        <div>
          <div style={{fontSize: '10px', textAlign: 'right'}}>{this.state.mode}</div>
          <div>{this.state.msg}</div>
          <a onClick={this.handleContinueToApp}>Continue to the app</a>
        </div>
      )
    }

    if (this.state.mode === GatheringInviteModes.ASK_FOR_TOKEN_RESPOND && this.state.gatheringInvite) {
      return (
        <div>
          <div style={{fontSize: '10px', textAlign: 'right'}}>{this.state.mode}</div>
          <div>
            <strong>Would you like to join {this.state.gathering.title}?</strong>
          </div>
          <div>
            <a onClick={this.respondOnToken.bind(this, GatheringInviteRespond.ACCEPTED)}>Yes</a> <a onClick={this.respondOnToken.bind(this, GatheringInviteRespond.REJECTED)}>No</a>
          </div>
          <div>{this.state.msg}</div>
        </div>
      )
    }

    return (
      <div>
        <div style={{fontSize: '10px', textAlign: 'right'}}>{this.state.mode}</div>
        <div>{this.state.msg}</div>
      </div>
    )
  }
}

GatheringInvite.contextTypes = {
  router: PropTypes.object.isRequired
}

GatheringInvite.propTypes = {
  match: PropTypes.object.isRequired,
  areacodes: PropTypes.array.isRequired,
  checkBelonging: PropTypes.func.isRequired,
  loginViaInvite: PropTypes.func.isRequired,
  registerViaInvite: PropTypes.func.isRequired,
  respondOnInvite: PropTypes.func.isRequired,
  setSession: PropTypes.func.isRequired
}

const mapStoreToProps = function(store) {
  return {
    areacodes: store.areacodesStore
  }
}

const mapDispatchToProps = dispatch => {
  return {
    checkBelonging: (token) => {
      return checkBelonging(token)
    },
    loginViaInvite: (areacode, phone, token) => {
      return loginViaInvite(areacode, phone, token)
    },
    registerViaInvite: (areacode, phone, token) => {
      return registerViaInvite(areacode, phone, token)
    },
    respondOnInvite: (token, respondType) => {
      return respondOnInvite(token, respondType)
    },
    setSession: (session) => {
      dispatch(setSession(session))
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(GatheringInvite)
