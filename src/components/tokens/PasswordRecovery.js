import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { memorizeAuthData, checkRecoveryToken, passwordSet } from '../../actions/Auth'
import { connect } from 'react-redux'
import { PasswordRecoveryModes } from '../../Constants'

class PasswordRecovery extends Component {

  constructor() {
    super()
    this.state = {
      mode: PasswordRecoveryModes.INITIALIZATION,
      msg: 'Processing token in progress...'
    }

    this.handleSetPassword = this.handleSetPassword.bind(this)
  }

  componentDidMount() {
    this.props.checkRecoveryToken(this.props.match.params.token).then(rsp => {
      if (rsp.status === 'ok') {
        this.setState({
          mode: PasswordRecoveryModes.TOKEN_VALID,
          msg: ''
        })
      } else {
        this.setState({
          msg: rsp.msg
        })
      }
    })
  }

  handleSetPassword(e) {
    if (e.keyCode && e.keyCode !== 13) {
      return
    }

    this.setState({
      msg: ''
    })

    this.props.passwordSet(this.props.match.params.token, this.refs['new-password'].value, this.refs['password-repeat'].value).then(rsp => {
      if (rsp.status === 'ok') {
        this.setState({
          mode: PasswordRecoveryModes.NEW_PASSWORD_SET,
          msg: rsp.msg
        })

        this.props.memorizeAuthData(rsp.data.user.areacode, rsp.data.user.phone)

      } else {
        this.setState({
          msg: rsp.msg
        })
      }
    })
  }

  render() {
    if (this.state.mode === PasswordRecoveryModes.TOKEN_VALID) {
      return (
        <div>
          <div>
            <label>
              <span>New Password</span>
              <input className="form-block" type="password" placeholder="New Password" ref="new-password" onKeyDown={this.handleSetPassword} />
            </label>
          </div>
          <div>
            <label>
              <span>Password Repeat</span>
              <input className="form-block" placeholder="Password Repeat" type="password" ref="password-repeat" onKeyDown={this.handleSetPassword} />
            </label>
          </div>
          <div>
            <a className="pretty-btn" onClick={this.handleSetPassword}>Set Password</a>
          </div>
          <div>{this.state.msg}</div>
        </div>
      )
    }

    if (this.state.mode === PasswordRecoveryModes.NEW_PASSWORD_SET) {
      return (
        <div>
          <div>{this.state.msg}</div>
          <div>You can proceed to <Link to="/login">login</Link> now.</div>
        </div>
      )
    }

    return (
      <div>{this.state.msg}</div>
    )
  }
}

PasswordRecovery.contextTypes = {
  router: PropTypes.object.isRequired
}

PasswordRecovery.propTypes = {
  match: PropTypes.object.isRequired,
  memorizeAuthData: PropTypes.func.isRequired,
  checkRecoveryToken: PropTypes.func.isRequired,
  passwordSet: PropTypes.func.isRequired
}

const mapDispatchToProps = dispatch => {
  return {
    memorizeAuthData: (areacode, phone, password) => {
      dispatch(memorizeAuthData(areacode, phone, password))
    },
    checkRecoveryToken: (token) => {
      return checkRecoveryToken(token)
    },
    passwordSet: (token, newPassword, passwordRepeat) => {
      return passwordSet(token, newPassword, passwordRepeat)
    }
  }
}

export default connect(null, mapDispatchToProps)(PasswordRecovery)
