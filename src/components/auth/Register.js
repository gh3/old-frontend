import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { PopupTypes } from '../../Constants'
import { connect } from 'react-redux'
import { register, memorizeAuthData, setSession } from '../../actions/Auth'
import { openPopup, closePopup } from '../../actions/Popup'

class Register extends Component {

  constructor() {
    super()
    this.state = {
      msg: ''
    }

    this.postRegister = this.postRegister.bind(this)
    this.handleRegister = this.handleRegister.bind(this)
    this.openConfirmation = this.openConfirmation.bind(this)
    this.handleAuthData = this.handleAuthData.bind(this)
  }

  componentDidMount() {
    if (this.props.auth.areacode) {
      this.refs.areacode.value = this.props.auth.areacode
    }
    if (this.props.auth.phone) {
      this.refs.phone.value = this.props.auth.phone
    }
    if (this.props.auth.password) {
      this.refs.password.value = this.props.auth.password
    }
  }

  postRegister(fromPopup) {
    const doublecheck = fromPopup ? true : false

    this.props.register(this.refs.areacode.value, this.refs.phone.value, this.refs.password.value, doublecheck).then(rsp => {
      if (rsp.status === 'ok') {
        this.props.setSession(rsp.data.session)
        this.props.history.push('/app')

        if (fromPopup) {
          this.props.closePopup()
        }

      } else if (rsp.status === 'error' && rsp.error === 'doublecheck') {
        this.openConfirmation(rsp.data.phoneSMS)

      } else {
        if (fromPopup) {
          this.props.closePopup()
        }

        this.setState({
          msg: rsp.msg
        })
      }
    })
  }

  handleRegister(e) {
    if (e.keyCode && e.keyCode !== 13) {
      return
    }

    this.setState({
      msg: ''
    })

    this.postRegister(false)
  }

  openConfirmation(phoneSMS) {

    const buttons = [
      {
        order: 1,
        caption: 'OK',
        callback: () => {
          this.postRegister(true)
        }
      }, {
        order: 2,
        caption: 'Edit',
        callback: () => {
          this.props.closePopup()
        }
      }
    ]

    const popupData = {
      phoneSMS: phoneSMS
    }

    this.props.openPopup(PopupTypes.REGISTRATION_DOUBLECHECK, buttons, popupData)
  }

  handleAuthData() {
    this.props.memorizeAuthData(this.refs.areacode.value, this.refs.phone.value, this.refs.password.value)
  }

  renderAreacodes() { 
    let areacodesOptions = this.props.areacodes.map((areacode) => { 
      return ( 
        <option key={areacode._id} value={areacode.areacode}>{areacode.title}</option> 
      ) 
    }) 
 
    return ( 
      <select className="form-block" ref="areacode" onChange={this.handleAuthData}>{areacodesOptions}</select> 
    ) 
  }

  render() {
    return (
      <div>
        <div>
          <label>
            <span>Areacode</span>
            {this.renderAreacodes()} 
          </label>
        </div>
        <div>
          <label>
            <span>Phone</span>
            <input className="form-block" type="tel" placeholder="Phone" ref="phone" onKeyDown={this.handleRegister} onBlur={this.handleAuthData} />
          </label>
        </div>
        <div>
          <label>
            <span>Password</span>
            <input className="form-block" type="password" placeholder="Password" ref="password" onKeyDown={this.handleRegister} onBlur={this.handleAuthData} />
          </label>
        </div>
        <div>

          <a className="pretty-btn" onClick={this.handleRegister}>Register</a>
        </div>
        <div>{this.state.msg}</div>
      </div>
    )
  }
}

Register.propTypes = {
  auth: PropTypes.object.isRequired,
  areacodes: PropTypes.array.isRequired,
  register: PropTypes.func.isRequired,
  setSession: PropTypes.func.isRequired,
  openPopup: PropTypes.func.isRequired,
  closePopup: PropTypes.func.isRequired,
  memorizeAuthData: PropTypes.func.isRequired
}

const mapStoreToProps = function(store) {
  return {
    auth: store.userStore.auth,
    areacodes: store.areacodesStore
  }
}

const mapDispatchToProps = dispatch => {
  return {
    register: (areacode, phone, password, doublecheck) => {
      return register(areacode, phone, password, doublecheck)
    },
    setSession: (session) => {
      dispatch(setSession(session))
    },
    openPopup: (popupType, buttons, data) => {
      dispatch(openPopup(popupType, buttons, data))
    },
    closePopup: () => {
      dispatch(closePopup())
    },
    memorizeAuthData: (areacode, phone, password) => {
      dispatch(memorizeAuthData(areacode, phone, password))
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(Register)
  