import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { memorizeAuthData, recoverPassword } from '../../actions/Auth'

class Password extends Component {

  constructor() {
    super()
    this.state = {
      msg: ''
    }

    this.handleRecover = this.handleRecover.bind(this)
    this.handleAuthData = this.handleAuthData.bind(this)
  }

  componentDidMount() {
    if (this.props.auth.areacode) {
      this.refs.areacode.value = this.props.auth.areacode
    }
    if (this.props.auth.phone) {
      this.refs.phone.value = this.props.auth.phone
    }
  }

  handleRecover(e) {
    if (e.keyCode && e.keyCode !== 13) {
      return
    }

    this.setState({
      msg: ''
    })

    this.props.recoverPassword(this.refs.areacode.value, this.refs.phone.value).then(rsp =>{
      this.setState({
        msg: rsp.msg
      })

      if (rsp.status === 'ok') {
        this.refs.phone.value = ''
      }
    })
  }

  handleAuthData() {
    this.props.memorizeAuthData(this.refs.areacode.value, this.refs.phone.value)
  }

  renderAreacodes() {
    let areacodesOptions = this.props.areacodes.map((areacode) => {
      return (
        <option key={areacode._id} value={areacode.areacode}>{areacode.title}</option>
      )
    })

    return (
      <select className="form-block" ref="areacode" onChange={this.handleAuthData}>{areacodesOptions}</select>
    )
  }

  render() {
    return (
      <div>
        <div>
          <label>
            <span>Areacode</span>
            {this.renderAreacodes()} 
          </label>
        </div>
        <div>
          <label>
            <span>Phone</span>
            <input className="form-block" type="tel" placeholder="Phone" ref="phone" onKeyDown={this.handleRecover} onBlur={this.handleAuthData} />
          </label>
        </div>
        <div>
          <a className="pretty-btn" onClick={this.handleRecover}>Recover password</a>
        </div>
        <div>{this.state.msg}</div>
        <div style={{paddingTop: '10px'}}>
          <small>
            <Link to="/login">Back to Login</Link>
          </small>
        </div>
      </div>
    )
  }
}

Password.propTypes = {
  auth: PropTypes.object.isRequired,
  areacodes: PropTypes.array.isRequired,
  memorizeAuthData: PropTypes.func.isRequired,
  recoverPassword: PropTypes.func.isRequired
}

const mapStoreToProps = function(store) {
  return {
    auth: store.userStore.auth,
    areacodes: store.areacodesStore
  }
}

const mapDispatchToProps = dispatch => {
  return {
    memorizeAuthData: (areacode, phone, password) => {
      dispatch(memorizeAuthData(areacode, phone, password))
    },
    recoverPassword: (areacode, phone) => {
      return recoverPassword(areacode, phone)
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(Password)
