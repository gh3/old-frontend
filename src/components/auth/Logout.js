import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { unsetSession } from '../../actions/Auth'

class Logout extends Component {

  componentWillMount() {
    this.props.unsetSession()
    this.props.history.push('/login')
  }

  render() {
    return (
      <div>Logout in process...</div>
    )
  }
}

Logout.propTypes = {
  unsetSession: PropTypes.func.isRequired
}


const mapDispatchToProps = dispatch => {
  return {
    unsetSession: () => {
      dispatch(unsetSession())
    }
  }
}

export default connect(null, mapDispatchToProps)(Logout)
