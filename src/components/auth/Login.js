import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { login, memorizeAuthData, setSession } from '../../actions/Auth'

class Login extends Component {

  constructor() {
    super()
    this.state = {
      msg: ''
    }

    this.handleLogin = this.handleLogin.bind(this)
    this.handleAuthData = this.handleAuthData.bind(this)
  }

  componentDidMount() {
    if (this.props.auth.areacode) {
      this.refs.areacode.value = this.props.auth.areacode
    }
    if (this.props.auth.phone) {
      this.refs.phone.value = this.props.auth.phone
    }
    if (this.props.auth.password) {
      this.refs.password.value = this.props.auth.password
    }
  }

  handleLogin(e) {
    if (e.keyCode && e.keyCode !== 13) {
      return
    }

    this.setState({
      msg: ''
    })

    this.props.login(this.refs.areacode.value, this.refs.phone.value, this.refs.password.value).then(rsp => {
      if (rsp.status === 'ok') {
        this.props.setSession(rsp.data.session)
        this.props.history.push('/app')
      } else {
        this.setState({
          msg: rsp.msg
        })
      }
    })
  }

  handleAuthData() {
    this.props.memorizeAuthData(this.refs.areacode.value, this.refs.phone.value, this.refs.password.value)
  }

  renderAreacodes() {
    let areacodesOptions = this.props.areacodes.map((areacode) => {
      return (
        <option key={areacode._id} value={areacode.areacode}>{areacode.title}</option>
      )
    })

    return (
      <select className="form-block" ref="areacode" onChange={this.handleAuthData}>{areacodesOptions}</select>
    )
  }

  render() {
    return (
      <div>
        <div>
          <label>
            <span>Areacode</span>
            {this.renderAreacodes()}
          </label>
        </div>
        <div>
          <label>
            <span>Phone</span>
            <input className="form-block" type="tel" placeholder="Phone" ref="phone" onKeyDown={this.handleLogin} onBlur={this.handleAuthData} />
          </label>
        </div>
        <div>
          <label>
            <span>Password</span>
            <input className="form-block" type="password" placeholder="Password" ref="password" onKeyDown={this.handleLogin} onBlur={this.handleAuthData} />
          </label>
        </div>
        <div>
          <a className="pretty-btn" onClick={this.handleLogin}>Login</a>
        </div>
        <div>{this.state.msg}</div>
        <div style={{paddingTop: '10px'}}>
          <small>
            <Link to="/password">Forgotten password?</Link>
          </small>
        </div>
      </div>
    )
  }
}

Login.propTypes = {
  auth: PropTypes.object.isRequired,
  areacodes: PropTypes.array.isRequired,
  login: PropTypes.func.isRequired,
  setSession: PropTypes.func.isRequired,
  memorizeAuthData: PropTypes.func.isRequired
}

const mapStoreToProps = function(store) {
  return {
    auth: store.userStore.auth,
    areacodes: store.areacodesStore
  }
}

const mapDispatchToProps = dispatch => {
  return {
    login: (areacode, phone, password) => {
      return login(areacode, phone, password)
    },
    setSession: (session) => {
      dispatch(setSession(session))
    },
    memorizeAuthData: (areacode, phone, password) => {
      dispatch(memorizeAuthData(areacode, phone, password))
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(Login)
