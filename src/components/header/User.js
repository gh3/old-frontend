import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { requestUserData } from '../../actions/User'
import { openPopup } from '../../actions/Popup'
import { PopupTypes } from '../../Constants'

class User extends Component {

  constructor() {
    super()

    this.handleNameAsking = this.handleNameAsking.bind(this)
  }

  componentWillMount() {
    if (this.props.session) {
      this.props.requestUserData()
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.session && nextProps.session !== this.props.session) {
      this.props.requestUserData()
    }
  }

  handleNameAsking() {
    this.props.openPopup(PopupTypes.ASK_FOR_NAME)
  }

  render() {
    if (!this.props.whoami._id) {
      return null
    }

    let user
    if (this.props.whoami.name) {
      user = this.props.whoami.name
    } else {
      user = <a onClick={this.handleNameAsking}>Snowflake</a>
    }

    return (
      <span style={{ float: 'right' }}>{ user }</span>
    )
  }
}

User.propTypes = {
  session: PropTypes.string.isRequired,
  whoami: PropTypes.object.isRequired,
  requestUserData: PropTypes.func.isRequired,
  openPopup: PropTypes.func.isRequired
}

const mapStoreToProps = function(store) {
  return {
    session: store.userStore.session,
    whoami: store.userStore.whoami
  }
}

const mapDispatchToProps = dispatch => {
  return {
    requestUserData: () => {
      dispatch(requestUserData())
    },
    openPopup: (popupType) => {
      dispatch(openPopup(popupType))
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(User)
