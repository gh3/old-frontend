import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

class Menu extends Component {

  renderOptionsButton() {
    if (this.props.gathering._id) {
      return (
        <span>
          <span> | </span>
          <Link to={'/gathering/' + this.props.gathering._id + '/options'}>Options</Link>
        </span>
      )
    }

    return null
  }

  renderAppMenu() {
    return (
      <div>                    
        <Link to="/app">Home</Link>
        <span> | </span>
        <Link to="/trash">Trash</Link>
        {this.renderOptionsButton()}
        <span> | </span>
        <Link to="/logout">Logout</Link>
        <hr />
      </div>
    )
  }

  renderBaseMenu() {
    return (
      <div>
        <Link to="/login">Login</Link>
        <span> | </span>
        <Link to="/register">Register</Link>
      <hr />
      </div>
    )
  }

  render() {
    if (this.props.session) {
      return (
        this.renderAppMenu()
      )
    }

    return (
      this.renderBaseMenu()
    )
  }
}

Menu.propTypes = {
  session: PropTypes.string.isRequired,
  gathering: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
  return {
    session: store.userStore.session,
    gathering: store.gatheringStore.gathering
  }
}

export default connect(mapStoreToProps)(Menu)
