import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom'
import { checkSession } from './actions/User'
import { requestAreacodes } from './actions/Areacodes'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Popup from './components/popup/Popup'
import Menu from './components/header/Menu'
import Header from './components/header/Header'
import routes from './Routes'
import Logout from './components/auth/Logout'

class Root extends Component {

  componentWillMount() {
    this.props.checkSession()
    this.props.requestAreacodes()
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.user.isChecked !== nextProps.user.isChecked) {
      this.redirectRoot(nextProps)
    }
  }

  redirectRoot(props) {
    if (props.location.pathname === '/') {
      if (props.user.session) {
        props.history.push('/app')
      } else {
        props.history.push('/login')
      }
    }
  }

  renderRoute(route) {
    if (route.type === 'SESSION' && !this.props.user.session) {
      return (
        <Route key={route.id} path={route.path} component={Logout} exact={!!route.exact} />
      )
    }

    return (
      <Route key={route.id} path={route.path} component={route.component} exact={!!route.exact} />
    )
  }

  render() {
    if (!this.props.user.isChecked) {
      return null
    }

    if (this.props.cordova.cameraRunning) {
      return (
        <div className="photo-shooter">
          <h1>PhotoShooter</h1>
        </div>
      )
    }

    return (
      <div className="main-layout">
        <Header />
        <Menu />
        <Switch>
          {routes.map(route => this.renderRoute(route))}
        </Switch>
        <Popup />
      </div>
    )
  }
}

Root.propTypes = {
  user: PropTypes.object.isRequired,
  checkSession: PropTypes.func.isRequired,
  requestAreacodes: PropTypes.func.isRequired
}

const mapStoreToProps = function(store) {
  return {
    user: store.userStore,
    cordova: store.cordovaStore
  }
}

const mapDispatchToProps = dispatch => {
  return {
    checkSession: () => {
      dispatch(checkSession())
    },
    requestAreacodes: () => {
      dispatch(requestAreacodes())
    }
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(withRouter(Root))
