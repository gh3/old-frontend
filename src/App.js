import React, { Component } from 'react';
import { Provider } from 'react-redux'
import store from './Store'
import { HashRouter, Route } from 'react-router-dom'
import Root from './Root'

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <HashRouter>
          <Route path="/" component={Root} />
        </HashRouter>
      </Provider>
    )
  }
}

export default App
