// tokens
import TokenGatheringInvite     from './components/tokens/GatheringInvite'
import TokenPasswordRecovery    from './components/tokens/PasswordRecovery'

// auth
import Login                    from './components/auth/Login'
import Register                 from './components/auth/Register'
import Password                 from './components/auth/Password'
import Logout                   from './components/auth/Logout'

// app
import Settings                 from './components/app/Settings'
import Gathering                from './components/app/Gathering'
import Dashboard                from './components/app/Dashboard'
import Trash                    from './components/app/Trash'
import NotFound                 from './components/app/NotFound'

// gathering
import Photos                   from './components/gathering/Photos'
import Options                  from './components/gathering/Options'
import Preferences              from './components/gathering/Preferences'
import Participants             from './components/gathering/Participants'
import Invites                  from './components/gathering/Invites'

export default [
  {
    id: 'dashboard',
    path: '/app',
    component: Dashboard,
    type: 'SESSION'
  }, {
    id: 'settings',
    path: '/settings',
    component: Settings,
    type: 'SESSION'
  }, {
    id: 'gathering',
    path: '/gathering/:gatheringId',
    component: Gathering,
    type: 'SESSION',
    routes: [
      {
        id: 'gathering-photos',
        path: '/gathering/:gatheringId',
        component: Photos,
        type: 'SESSION',
        exact: true
      }, {
        id: 'gathering-options',
        path: '/gathering/:gatheringId/options',
        component: Options,
        type: 'SESSION'
      }, {
        id: 'gathering-preferences',
        path: '/gathering/:gatheringId/preferences',
        component: Preferences,
        type: 'SESSION'
      }, {
        id: 'gathering-participants',
        path: '/gathering/:gatheringId/participants',
        component: Participants,
        type: 'SESSION'
      }, {
        id: 'gathering-invites',
        path: '/gathering/:gatheringId/invites',
        component: Invites,
        type: 'SESSION'
      }
    ]
  }, {
    id: 'trash',
    path: '/trash',
    component: Trash,
    type: 'SESSION'
  }, {
    id: 'token-gathering-invite',
    path: '/gi/:token',
    component: TokenGatheringInvite,
    type: 'TOKEN'
  }, {
    id: 'token-password-recovery',
    path: '/pr/:token',
    component: TokenPasswordRecovery,
    type: 'TOKEN'
  }, {
    id: 'login',
    path: '/login',
    component: Login,
    type: 'REGULAR'
  }, {
    id: 'register',
    path: '/register',
    component: Register,
    type: 'REGULAR'
  }, {
    id: 'password',
    path: '/password',
    component: Password,
    type: 'REGULAR'
  }, {
    id: 'logout',
    path: '/logout',
    component: Logout,
    type: 'REGULAR'
  }, {
    id: 'not-found',
    path: '*',
    component: NotFound,
    type: 'REGULAR'
  }
]
