var fs = require('fs')
var file = '../cordova/www/index.html'
var indexHtml = fs.readFileSync(file, 'utf-8')
if (indexHtml.indexOf('cordova.js') > -1) {
  console.error("Possible error with including cordova.js script into index.html file!\n")
  return
}
var firstScriptLocation = indexHtml.indexOf('<script')
var cordovaScript = '<script type="text/javascript" src="cordova.js"></script>'
var newIndexHtml = indexHtml.slice(0, firstScriptLocation) + cordovaScript + indexHtml.slice(firstScriptLocation)
fs.writeFileSync(file, newIndexHtml, 'utf-8')
console.log("Successfully added cordova.js script to index.html file.\n")
