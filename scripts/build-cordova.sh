#!/bin/bash
react-scripts build
rm -r ../cordova/www/*
cp -r build/* ../cordova/www/
node scripts/addCordovaScriptTag.js
zip -r ../cordova/Gathering_`date +%Y%m%d_%H%M%S`.zip ../cordova/ -x \"../cordova/cordova*\" -x *.zip* -x *.git* -x *.DS_Store*
echo "Zip is ready for building with cordova/phonegap!"
