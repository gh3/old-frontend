This project is obsolete and not maintained anymore!

# ABOUT #

Code in this repository was an attempt to rewrite [GH3 Client](https://gitlab.com/gh3/old-client/). It uses modern javascript (ES6) approaches and [Create React App](https://github.com/facebookincubator/create-react-app) for building.

Basically it is the same app in addition to cordova build script, which was never really useful.
